import 'package:alpha/app/services/settings_services.dart';
import 'package:alpha/models/entity/notif_app.dart';
import 'package:custom_dialog/custom_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List<NotifApp> notifs = [];
  bool show = false;

  get_list_notif() async {
    var nots = await Settings_Services.get_all_notifications();
    if (!this.mounted) return;
    setState(() {
      notifs = nots;
      show = false;
      print(notifs);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_list_notif();
  }

  @override
  Widget build(BuildContext context) {
    Widget li = show == true
        ? Center(
            child: CupertinoTheme(
                data: CupertinoTheme.of(context)
                    .copyWith(brightness: Brightness.light),
                child: CupertinoActivityIndicator()))
        : ListView(
            children:
                List<Widget>.from(notifs.map((e) => NotifWidget(e)).toList()));

    return InkWell(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 22.h,
            ),
            Padding(
                padding: EdgeInsets.only(left: 26.w, right: 26.w),
                child: Text(
                  "notifications".tr().toUpperCase(),
                  style:
                      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w800),
                )),
            Container(
              height: 22.h,
            ),
            Expanded(
              child: li,
            )
          ],
        ),
        onTap: () {
         /* showDialog(
              context: context,
              builder: (context) => Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: CustomDialog(
                      content: Text(
                        'No Access',
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 20.0,
                        ),
                      ),
                      firstColor: Colors.white,
                      secondColor: Colors.white,
                      title: Text('Error'),
                    ),
                  ));*/
        });
  }
}

class NotifWidget extends StatelessWidget {
  NotifWidget(this.notif);

  NotifApp notif;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.all(6),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Color(0xffF0F2F5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SvgPicture.asset(notif.icon),
              Container(
                width: 8.w,
              ),
              Text(
                notif.title.toUpperCase(),
                style: TextStyle(fontWeight: FontWeight.w800),
              ),
              Expanded(
                child: Container(),
              ),
              Text(
                notif.date,
                style: TextStyle(
                    fontWeight: FontWeight.w500, color: Color(0xff807F82)),
              ),
            ],
          ),
          Container(
            height: 12.h,
          ),
          Container(child: Text(notif.description))
        ],
      ),
    );
  }
}
