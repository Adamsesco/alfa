import 'package:alpha/models/entity/fuel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FuelCard extends StatelessWidget {
  FuelCard(this.fuel);

  Fuel fuel;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 8.h),
        child: Row(
          children: [
            Container(
              height: 46.h,
              width: 4.w,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  color: fuel.color),
            ),
            Container(width: 24.w),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      fuel.type,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.sp),
                    ),
                    Container(
                      width: 8.w,
                    ),
                    Text(fuel.pourcent),
                    Container(
                      width: 4.w,
                    ),
                    SvgPicture.asset(fuel.up == true
                        ? "assets/icons/down.svg"
                        : "assets/icons/growth.svg")
                  ],
                ),
                Container(
                  height: 4.h,
                ),
                Row(
                  children: [
                    Text(fuel.type),
                    Container(
                      width: 8.w,
                    ),
                    Text(
                      "LAST UPDATE YESTERDAY: -10:32",
                      style: TextStyle(fontSize: 12.sp),
                    ),
                    Container(
                      width: 2.w,
                    ),
                  ],
                ),
              ],
            ),
            Expanded(child: Container()),
            Text(
              "10.222",
              style: TextStyle(fontSize: 22.sp, fontWeight: FontWeight.w600),
            ),
            Container(width: 16.w)
          ],
        ));
  }
}
