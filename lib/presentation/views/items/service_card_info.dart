import 'package:alpha/models/entity/station.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SeerviceItem extends StatelessWidget {
  SeerviceItem(this.service);

  Station service;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 245.w,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: service.color),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.only(
                  top: 30.w, left: 30.w, right: 30.w, bottom: 16.h),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      service.type.toUpperCase(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      height: 12.h,
                    ),
                    Container(
                      child: Text(
                        service.small_desc,
                        style:
                            TextStyle(color: Colors.white, fontSize: 10.0.sp),
                      ),
                    ),
                    Container(
                      height: 12.h,
                    ),
                    Container(
                      child: Text(
                        service.desc,
                        style:
                            TextStyle(color: Colors.white, fontSize: 10.0.sp),
                      ),
                    ),
                  ])),
          Expanded(child: Container()),
          Container(
              height: 248.h,
              width: 245.w,
              child: ClipRRect(
                  //<--clipping image
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(16.r),
                      bottomLeft: Radius.circular(16.r)),
                  child: Image.asset(
                    service.image_preview,
                    height: 248.h,
                    width: 245.w,
                    fit: BoxFit.cover,
                  )))
        ],
      ),
    );
  }
}
