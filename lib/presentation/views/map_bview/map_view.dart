import 'dart:async';
import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/views/filter/filter_page.dart';
import 'package:alpha/presentation/views/signin/signin.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class MapScreeen extends StatefulWidget {
  @override
  MapScreeenState createState() => MapScreeenState();
}

class MapScreeenState extends State<MapScreeen> {
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
  }

  double zoomVal = 5.0;

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);

    Widget _zoomplusfunction() {
      return Align(
          alignment: Alignment.topRight,
          child: Container(
              padding: EdgeInsets.all(10.w),
              child: GestureDetector(
                  onTap: () {
                    if (myProvider.isAuthenticate == false)
                      please_login();
                    else
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return FilterPage();
                      }));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.5.r)),
                    padding: EdgeInsets.all(16),
                    child: SvgPicture.asset("assets/icons/filter.svg"),
                  ))));
    }

    return Stack(
      children: <Widget>[
        _buildGoogleMap(context),
        _zoomplusfunction(),
        _buildContainer(),
      ],
    );
  }

  please_login() {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Text('Please login to conitnue.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return SignInScreen();
              }));
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
  }

  Widget _buildContainer() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 420.h,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: _boxes(
                "https://lh5.googleusercontent.com/p/AF1QipO3VPL9m-b355xWeg4MXmOQTauFAEkavSluTtJU=w225-h160-k-no",
                40.738380,
                -73.988426,
                "Gramercy Tavern"),
          )),
    );
  }

  Widget _boxes(String _image, double lat, double long, String restaurantName) {
    return GestureDetector(
      onTap: () {
        _gotoLocation(lat, long);
      },
      child: Container(
        width: 394.w,
        height: 380.h,
        padding: EdgeInsets.all(4.w),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: myDetailsContainer1(restaurantName),
        ),
      ),
    );
  }

  TextWidget(String name1, String name2) {
    return Padding(
        padding: EdgeInsets.only(left: 0.w),
        child: new RichText(
            text: new TextSpan(
                text: name1,
                style: new TextStyle(
                    color: ColorsApp.col_app,
                    height: 1.2,
                    fontWeight: FontWeight.w800,
                    fontSize: ScreenUtil().setSp(17)),
                children: <TextSpan>[
              new TextSpan(
                  text: name2,
                  style: new TextStyle(
                      color: ColorsApp.col_app,
                      fontWeight: FontWeight.w400,
                      fontSize: ScreenUtil().setSp(17))),
            ])));
  }

  Widget button(String title, Color color, String number) => Container(
      width: 102.w,
      child: RaisedButton(
          padding: EdgeInsets.all(5.w),
          elevation: 0,
          shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(2),
            ),
          ),
          color: color,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyle(
                    height: 1.2,
                    color: Colors.white,
                    fontFamily: context.locale == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",
                    fontWeight: FontWeight.w500,
                    fontSize: 15.0.sp),
              ),
              Expanded(child: Container(width: 8.w)),
              Padding(
                  padding: EdgeInsets.all(2),
                  child: CircleAvatar(
                    radius: 14.r,
                    backgroundColor: Colors.white,
                    child: Center(
                        child: Text(number,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 13.5.sp,
                                fontFamily: context.locale == Locale('ar', 'AR')
                                    ? "cairo bold"
                                    : "rubik medium"))),
                  ))
            ],
          ),
          onPressed: () {}));

  /* Widget border_widget(String title, Color color) => Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(12.5.r)),
      padding: EdgeInsets.all(11.w),
      child: Text(
        title,
        style: TextStyle(color: color,height: 1.2, fontWeight: FontWeight.w600),
      ));*/

  Widget border_widget2(String icon, Color color) => Container(
      /* decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(12.5.r)),*/
      padding: EdgeInsets.all(11.w),
      child: SvgPicture.asset(
        icon,
      ));

  Widget border_widget3(String icon, Color color) => Container(
      /* decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(12.5.r)),*/
      padding: EdgeInsets.all(11.w),
      child: Image.asset(
        icon,
        fit: BoxFit.contain,
      ));

  Widget myDetailsContainer1(String restaurantName) {
    return Card(
        elevation: 1,
        shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(12.w),
          ),
        ),
        child: Padding(
            padding: EdgeInsets.all(12.w),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextWidget(
                    "Alfa".toUpperCase(), "  Station khaldiya".toUpperCase()),
                Container(
                  height: 12.h,
                ),
                Text("Block 5, Ahmadi Ahmadi City, Kuwait"),
                Container(
                  height: 30.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    button("Ultra Super", ColorsApp.col_green_f, "98"),
                    Container(
                      width: 5.w,
                    ),
                    button("Super", ColorsApp.col_app_brown_f, "95"),
                    Container(
                      width: 5.w,
                    ),
                    button("Premium", ColorsApp.col_red, "91"),
                  ],
                ),
                Container(
                  height: 42.h,
                ),
                Row(
                  children: [
                    border_widget2(
                        "assets/images/ba9ala.svg", ColorsApp.col_green),
                    Container(
                      width: 8.w,
                    ),
                    border_widget3(
                        "assets/images/subway.png", ColorsApp.col_black),
                  ],
                ),
                Container(
                  height: 40.h,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                          width: 140.w,
                          child: PrimaryButton(
                              text: "direction".tr().toUpperCase(),
                              color_text: ColorsApp.col_app,
                              color: ColorsApp.col_app,
                              onTap: () {})),
                    ),
                    Container(
                      width: 24.w,
                    ),
                    Expanded(
                      child: Container(
                          width: 140.w,
                          child: PrimaryButton(
                              text: "send_location".tr().toUpperCase(),
                              color_text: ColorsApp.col_app,
                              color: ColorsApp.col_app,
                              onTap: () {})),
                    )
                  ],
                )
              ],
            )));
  }

  Widget _buildGoogleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition:
            CameraPosition(target: LatLng(40.712776, -74.005974), zoom: 12),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: {
          newyork1Marker,
          newyork2Marker,
          newyork3Marker,
          gramercyMarker,
          bernardinMarker,
          blueMarker
        },
      ),
    );
  }

  Future<void> _gotoLocation(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 15,
      tilt: 50.0,
      bearing: 45.0,
    )));
  }
}

Marker gramercyMarker = Marker(
  markerId: MarkerId('gramercy'),
  position: LatLng(40.738380, -73.988426),
  infoWindow: InfoWindow(title: 'Gramercy Tavern'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueOrange,
  ),
);

Marker bernardinMarker = Marker(
  markerId: MarkerId('bernardin'),
  position: LatLng(40.761421, -73.981667),
  infoWindow: InfoWindow(title: 'Le Bernardin'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueOrange,
  ),
);
Marker blueMarker = Marker(
  markerId: MarkerId('bluehill'),
  position: LatLng(40.732128, -73.999619),
  infoWindow: InfoWindow(title: 'Blue Hill'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueOrange,
  ),
);

//New York Marker

Marker newyork1Marker = Marker(
  markerId: MarkerId('newyork1'),
  position: LatLng(40.742451, -74.005959),
  infoWindow: InfoWindow(title: 'Los Tacos'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueOrange,
  ),
);
Marker newyork2Marker = Marker(
  markerId: MarkerId('newyork2'),
  position: LatLng(40.729640, -73.983510),
  infoWindow: InfoWindow(title: 'Tree Bistro'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueOrange,
  ),
);
Marker newyork3Marker = Marker(
  markerId: MarkerId('newyork3'),
  position: LatLng(40.719109, -74.000183),
  infoWindow: InfoWindow(title: 'Le Coucou'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueOrange,
  ),
);
