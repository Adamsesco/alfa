import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/views/bottom_navigation/bottom_navigation_screen.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ChooseLanguage extends StatefulWidget {
  const ChooseLanguage({Key key}) : super(key: key);

  @override
  _ChooseLanguageState createState() => _ChooseLanguageState();
}

class _ChooseLanguageState extends State<ChooseLanguage> {
  bool choose_eng = true;
  bool choose_ar = false;

  eng() {
    context.locale = Locale('en', 'US');
    setState(() {
      choose_eng = true;
      choose_ar = false;
    });
  }

  arab() {
    context.locale = Locale('ar', 'AR');
    setState(() {
      choose_ar = true;
      choose_eng = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget widget_item(
            String text, Color color, String icon, onTap, color_text) =>
        InkWell(
            onTap: () {
              onTap();
            },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 262.w,
                    decoration: BoxDecoration(
                        color: color,
                        borderRadius: BorderRadius.circular(12.5.r)),
                    padding: EdgeInsets.only(top: 12.h, bottom: 12.h),
                    child: Row(
                      children: [
                        Container(
                          width: 36.w,
                        ),
                        Text(
                          text,
                          style: TextStyle(
                              height: 1.2,
                              color: color_text,
                              fontFamily: context.locale == Locale('ar', 'AR')
                                  ? "cairo bold"
                                  : "rubik medium",
                              fontWeight: FontWeight.w400,
                              fontSize: 17.5.sp),
                        ),
                        Expanded(child: Container()),
                        Image.asset(
                          icon,
                          height: ScreenUtil().setWidth(40),
                          width: ScreenUtil().setWidth(40),
                          fit: BoxFit.cover,
                        ),
                        Container(
                          width: 24.w,
                        ),
                      ],
                    ),
                  )
                ]));

    return Scaffold(
      body: ListView(
        children: [
          Stack(
            children: [
              ShaderMask(
                shaderCallback: (rect) {
                  return LinearGradient(
                    begin: Alignment.center,
                    end: Alignment.bottomCenter,
                    colors: [Colors.black, Colors.transparent],
                  ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
                },
                blendMode: BlendMode.dstIn,
                child: Image.asset(
                  'assets/images/back.png',
                  height: 450.h,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                  bottom: 32.h,
                  left: 12,
                  right: 12,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: SvgPicture.asset(
                      "assets/images/logo.svg",
                      width: 120.w,
                    ),
                  ))
            ],
          ),
          Center(
            child: Text(
              "chse".tr(),
              textAlign: TextAlign.center,
              style: TextStyle(color: ColorsApp.col_app),
            ),
          ),
          Container(height: 45.h),
          widget_item(
              "ENGLISH",
              choose_eng == true ? ColorsApp.col_app : ColorsApp.col_grey_clear,
              "assets/icons/english.png",
              eng,
              choose_eng == true ? Colors.white : ColorsApp.col_grey_text),
          Container(
            height: 18.h,
          ),
          widget_item(
              "ARABIC",
              choose_ar == true ? ColorsApp.col_app : ColorsApp.col_grey_clear,
              "assets/icons/arabic.png",
              arab,
              choose_ar == true ? Colors.white : ColorsApp.col_grey_text),
          Container(
            height: 12.h,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
                width: 142.w,
                child: PrimaryButton(
                    text: "continue".tr().toUpperCase(),
                    color_text: ColorsApp.col_app,
                    color: ColorsApp.col_app,
                    onTap: () {
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return BottomNavigationScreen();
                      }));
                    }))
          ])
        ],
      ),
    );
  }
}
