import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/views/bottom_navigation/bottom_navigation_screen.dart';
import 'package:alpha/presentation/views/signin/signin.dart';
import 'package:alpha/presentation/views/slides/choose_language.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
class OnBoard extends StatefulWidget {
  @override
  _OnBoardState createState() => _OnBoardState();
}

class _OnBoardState extends State<OnBoard> {
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  Color gradientStart = Colors.transparent;
  Color gradientEnd = Colors.black87;

  final header = TextStyle(
    color: Colors.white,
    fontSize: 20.0.sp,
    height: 1.2,

    fontWeight: FontWeight.bold,
  );

  final subheading = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w300,
    height: 1.2,
    fontSize: 17.0.sp,
  );

  final sub = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w500,
    height: 1.2,

    fontSize: 13.8.sp,
  );

  Widget getField(headerTxt, subtitle, descTxt, imageSrc) {
    return Stack(children: [
      ShaderMask(
        shaderCallback: (rect) {
          return LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [gradientStart, gradientEnd],
          ).createShader(Rect.fromLTRB(0, -80, rect.width, rect.height - 20));
        },
        blendMode: BlendMode.darken,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage(imageSrc),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      Container(
        // decoration: getDecoration(imageSrc),
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(),
            ),
            Container(
              child: Text(
                headerTxt.toString().toUpperCase(),
                style: header,
              ),
            ),
            Container(
              child: Text(
                subtitle.toString().toUpperCase(),
                style: subheading,
              ),
            ),
            Container(
              height: 26.h,
            ),
            Text(
              descTxt,
              style: subheading,
            ),
            Container(
              height: 130.h,
            ),
          ],
        ),
      ),

      /* _currentPage == _numPages - 1
              ? Expanded(
            child: Align(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding:
                      EdgeInsets.symmetric(horizontal: 15.0),
                      child: Text(
                        'Skip',
                        style: TextStyle(
                            color: Colors.white, fontSize: 24.0),
                      ),
                    ),
                    getButton(_pageController),
                  ],
                ),
              ),
            ),
          )
              : Text('')*/

      Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              _currentPage == _numPages - 1
                  ? Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                      SizedBox(
                          height: 54.h,
                          width: 189.w,
                          child: PrimaryButton(
                              text: "get_started".tr().toUpperCase(),
                              color_text: ColorsApp.col_app,
                              color: ColorsApp.col_app,
                              onTap: () {
                                Navigator.push(context,
                                    new MaterialPageRoute(builder: (BuildContext context) {
                                      return /*SignInScreen*/ChooseLanguage()/*BottomNavigationScreen()*/;
                                    }));
                              }))
                    ])
                  : Container(),
              Container(height: 36.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildPageIndicator(),
              ),
            ],
          ))
    ]);
  }

  Widget getButton(_pageController) {
    return FlatButton(
      onPressed: () {
        _pageController.nextPage(
            duration: Duration(microseconds: 300), curve: Curves.easeIn);
      },
      child: Text(
        'Next',
        style: TextStyle(color: Colors.white, fontSize: 24.0),
      ),
    );
  }

  BoxDecoration getDecoration(image) {
    return BoxDecoration(
      image: DecorationImage(
        image: AssetImage(image),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget indicator(bool isActive) {
    return Padding(
        padding: EdgeInsets.only(bottom: 24.h),
        child: AnimatedContainer(
          duration: Duration(microseconds: 150),
          margin: EdgeInsets.symmetric(horizontal: 8.0),
          height: 9.0.h,
          width: isActive ? 24.0 : 9.0.h,
          decoration: BoxDecoration(
            color: isActive ? Colors.white : Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
        ));
  }

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? indicator(true) : indicator(false));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsApp.col_black,
      body: PageView(
        physics: ClampingScrollPhysics(),
        controller: _pageController,
        onPageChanged: (int page) {
          setState(() {
            _currentPage = page;
          });
        },
        children: <Widget>[
          getField(
              "welcome".tr(),
              'alfa_app'.tr(),
              "lorem".tr(),
              "assets/images/back1.png"),
          getField(
              'leading'.tr(),
              'marketing_company'.tr(),
              "lorem".tr(),
              "assets/images/back2.png"),
          getField(
              'solution_for'.tr(),
              'all_quick'.tr(),
              "lorem".tr(),
              "assets/images/back3.png")
        ],
      ),
    );
  }
}
