import 'dart:io';

import 'package:alpha/app/services/rest_services.dart';
import 'package:alpha/app/services/validators.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/custom_textfield.dart';
import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignUpPhone extends StatefulWidget {
  @override
  _SignUpPhoneState createState() => _SignUpPhoneState();
}

class _SignUpPhoneState extends State<SignUpPhone> {
  final _passcontroller = new TextEditingController();
  final _cpasscontroller = new TextEditingController();
  final _emailcontroller = new TextEditingController();
  final _firstcontroller = new TextEditingController();
  final _lastcontroller = new TextEditingController();
  final _phonecontroller = new TextEditingController();

  FocusNode _focusemail = new FocusNode();
  FocusNode _focusfisrt = new FocusNode();
  FocusNode _focuslast = new FocusNode();
  FocusNode _focuscpassword = new FocusNode();
  FocusNode _focuspassword = new FocusNode();
  FocusNode _focusphone = new FocusNode();

  RestService restservice = new RestService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  AppService ap = new AppService();
  bool load = false;

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void _handleSubmitted() async {
    //  var token = await      _firebaseMessaging.getToken();

    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      // showInSnackBar("");
    } else {
      setState(() {
        show = true;
      });
      form.save();
      /**/
      String name = _emailcontroller.text;
      String pass = _passcontroller.text;
      String firstname = _firstcontroller.text;
      String lastname = _lastcontroller.text;
      String phone = _phonecontroller.text;

      print({
        "first_name": "$firstname",
        "last_name": "$lastname",
        "email": "$name",
        "password": "$pass",
        "phone": "$phone",
        "avatar":""
      });

      var a = await restservice.post_signup("signup", context, {
        "first_name": "$firstname",
        "last_name": "$lastname",
        "email": "$name",
        "password": "$pass",
        "phone": "$phone",
        "avatar":""

      });



      setState(() {
        show = false;
      });

      if (a == false) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("Error "),
          duration: new Duration(seconds: 4),
        ));
      } else {
        Navigator.pop(context);
        //Routes.goto(context, "login_new")
        //Routes.gof(context,_tel.text, widget.onLocaleChange,func: widget.func);

      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Padding(
              padding: EdgeInsets.all(16),
              child: SvgPicture.asset(
                "assets/images/logo.svg",
                width: 110.w,
              )),
          elevation: 0,
          leading: ArrowButton(),
          actions: [
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(
                "assets/icons/notif.svg",
                color: ColorsApp.col_app,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset("assets/icons/wb.svg"),
            ),
          ]),
      body:  Form(
    key: _formKey,
    child:ListView(
        padding: EdgeInsets.only(left: 32.w, right: 32.w, top: 32.h),
        children: [
          CustomTexts.TextWidget4(
              context.locale == Locale('ar', 'AR')
                  ? "create_account".tr()
                  : "create_account".tr().split(" ")[0],
              "create_account".tr().split(" ")[1]),
          Container(
            height: 11.h,
          ),
          Container(
              child: new RichText(
            text: new TextSpan(
              text: "login_info".tr(),
              style: new TextStyle(color: ColorsApp.col_black),
              children: <TextSpan>[],
            ),
          )),
          Container(
            height: 46.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 16.w),
              child: Theme(
                  data: ThemeData(
                    hintColor: ColorsApp.col_gr_back,
                  ),
                  child: TextFieldWidget(
                    _firstcontroller,
                    _focusfisrt,
                    "firstname".tr().toUpperCase(),
                    false,
                    wid: Container(),
                  ))),
          Container(
            height: 10.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 16.w),
              child: Theme(
                  data: ThemeData(
                    hintColor: ColorsApp.col_gr_back,
                  ),
                  child: TextFieldWidget(
                    _lastcontroller,
                    _focuslast,
                    "lastname".tr().toUpperCase(),
                    false,
                    wid: Container(),
                  ))),
          Container(
            height: 10.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 16.w),
              child: Theme(
                  data: ThemeData(
                    hintColor: ColorsApp.col_gr_back,
                  ),
                  child: TextFieldWidget(
                    _emailcontroller,
                    _focusemail,
                    "email_address".tr().toUpperCase(),
                    false,
                    wid: Container(),
                  ))),
          Container(
            height: 10.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 16.w),
              child: Theme(
                  data: ThemeData(
                    hintColor: ColorsApp.col_gr_back,
                  ),
                  child: TextFieldWidget(
                    _phonecontroller,
                    _focusphone,
                    "Phone".tr().toUpperCase(),
                    false,
                    wid: Container(),
                  ))),
          Container(
            height: 10.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 16.w),
              child: Theme(
                  data: ThemeData(
                    hintColor: ColorsApp.col_gr_back,
                  ),
                  child: TextFieldWidget(
                    _passcontroller,
                    _focuspassword,
                    "password".tr().toUpperCase(),
                    false,
                    wid: Container(),
                  ))),
          Container(
            height: 10.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 16.w),
              child: Theme(
                  data: ThemeData(
                    hintColor: ColorsApp.col_gr_back,
                  ),
                  child: TextFieldWidget(
                    _passcontroller,
                    _focuspassword,
                    "confirm_password".tr().toUpperCase(),
                    false,
                    wid: Container(),
                  ))),
          Container(
            height: 30.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, right: 0.w),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8.r)),
                        color: ColorsApp.col_white_grey),
                    child: Image.asset(
                      "assets/images/prof.png",
                      height: ScreenUtil().setWidth(60),
                      width: ScreenUtil().setWidth(60),
                      fit: BoxFit.cover,
                    )),
                Container(
                  width: 12.w,
                ),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 16.w),
                        padding: EdgeInsets.only(
                            left: 10.w, right: 10.w, top: 26.h, bottom: 26.h),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.r)),
                            color: ColorsApp.col_grey_clear),
                        child: Center(
                            child: Text(
                          "upload_picture".tr().toUpperCase(),
                          style: TextStyle(
                              fontSize: 15.5.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.w700),
                        ))))
              ])),
          Container(height: 16.h),
          Padding(
            padding: EdgeInsets.only(left: 16.w, right: 16.w),
            child: Row(
              children: [
                Container(
                    width: 100.w,
                    child: FlatButton(
                        onPressed: () {},
                        child: Text(
                          "CLEAR".tr().toUpperCase(),
                          style: TextStyle(
                              fontSize: 16.sp, color: ColorsApp.col_grey_text),
                        ))),
                Expanded(
                  child: Container(),
                ),
                SizedBox(
                    height: 54.h,
                    width: 159.w,
                    child: PrimaryButton(
                        isLoading: show ? true : false,
                        text: "create_account".tr().toUpperCase(),
                        color_text: ColorsApp.col_app,
                        color: ColorsApp.col_app,
                        onTap: () {
                          _handleSubmitted();

                          // Navigator.pop(context);
                          /* Navigator.push(context,
                                                      new MaterialPageRoute(builder: (BuildContext context) {
                                                        return RestrictScreen();
                                                      }));*/
                        })),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
