import 'dart:convert';
import 'dart:io';

import 'package:alpha/app/services/rest_services.dart';
import 'package:alpha/app/services/validators.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/custom_textfield.dart';
import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/bottom_navigation/bottom_navigation_screen.dart';
import 'package:alpha/presentation/views/signup/signnup_screen_phone.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _passcontroller = new TextEditingController();
  final _emailcontroller = new TextEditingController();
  FocusNode _focusemail = new FocusNode();
  FocusNode _focuspassword = new FocusNode();
  RestService restservice = new RestService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  AppService ap = new AppService();
  bool load = false;

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void _handleSubmitted() async {
    //  var token = await      _firebaseMessaging.getToken();

    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      // showInSnackBar("");
    } else {
      setState(() {
        show = true;
      });
      form.save();
      /**/
      String name = _emailcontroller.text;
      String pass = _passcontroller.text;

      var a = await restservice.post_login("login", context, {
        "email": "$name",
        "password": "$pass",
        "token": "",
        "device_type": Platform.isAndroid == true ? "android" : "ios"
      });

      setState(() {
        show = false;
      });

      /* if (a == 201) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("هذا الحساب غير مسجل"),
          duration: new Duration(seconds: 4),
        ));
      }

      else */

      print(a);

      if (a == 400) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content:
              new Text("This account is not registered or mot de passe ! "),
          duration: new Duration(seconds: 4),
        ));
      } else {
        //Routes.goto(context, "login_new")
        //Routes.gof(context,_tel.text, widget.onLocaleChange,func: widget.func);

      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Padding(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset(
                  "assets/images/logo.svg",
                  width: 110.w,
                )),
            elevation: 0,
            leading: ArrowButton(),
            actions: [
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset(
                  "assets/icons/notif.svg",
                  color: ColorsApp.col_app,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("assets/icons/wb.svg"),
              ),
            ]),
        body: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.only(left: 32.w, right: 32.w, top: 32.h),
            children: [
              CustomTexts.TextWidget4(
                  context.locale == Locale('ar', 'AR')
                      ? "signin".tr()
                      : "signin".split(" ")[0],
                  "signin".tr().split(" ")[1]),
              Container(
                height: 11.h,
              ),
              Container(
                  child: new RichText(
                text: new TextSpan(
                  text: "login_info".tr(),
                  style: new TextStyle(
                    color: ColorsApp.col_black,
                    fontFamily: context.locale == Locale('ar', 'AR')
                        ? "cairo"
                        : "rubik",
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => Navigator.push(context,
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) {
                                return SignUpPhone();
                              })),
                        text: "create_not".tr(),
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            color: ColorsApp.col_app)),
                  ],
                ),
              )),
              Container(
                height: 46.h,
              ),
              Padding(
                  padding: EdgeInsets.only(left: 16.w, right: 16.w),
                  child: Theme(
                      data: ThemeData(
                        hintColor: ColorsApp.col_gr_back,
                      ),
                      child: TextFieldWidget(
                        _emailcontroller,
                        _focusemail,
                        "email_address".tr().toUpperCase(),
                        false,
                        wid: Container(),
                      ))),
              Container(
                height: 10.h,
              ),
              Padding(
                  padding: EdgeInsets.only(left: 16.w, right: 16.w),
                  child: Theme(
                      data: ThemeData(
                        hintColor: ColorsApp.col_gr_back,
                      ),
                      child: TextFieldWidget(
                        _passcontroller,
                        _focuspassword,
                        "password".tr(),
                        false,
                        wid: Container(),
                      ))),
              Container(
                height: 30.h,
              ),
              Padding(
                padding: EdgeInsets.only(left: 16.w, right: 16.w),
                child: Row(
                  children: [
                    SizedBox(
                        height: 54.h,
                        width: 139.w,
                        child: PrimaryButton(
                            isLoading: show ? true : false,
                            text: "signin".tr().toUpperCase(),
                            color_text: ColorsApp.col_app,
                            color: ColorsApp.col_app,
                            onTap: () {
                              _handleSubmitted();
                              /*Navigator.push(context, new MaterialPageRoute(
                              builder: (BuildContext context) {
                            return BottomNavigationScreen();
                          }));*/
                            })),
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      "forgot".tr(),
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                          fontSize: 14.5.sp,
                          color: ColorsApp.col_app),
                    )
                  ],
                ),
              ),
              Container(height: 40.h),
              Padding(
                  padding: EdgeInsets.only(left: 16.w, right: 16.w),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                            padding: EdgeInsets.all(8.w),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.r)),
                                color: ColorsApp.col_white_grey),
                            child: Image.asset("assets/icons/face.png")),
                        Container(
                          width: 12.w,
                        ),
                        Text(
                          "face_id".tr().toUpperCase(),
                          style: TextStyle(
                              fontSize: 14.5.sp, color: ColorsApp.col_gr),
                        )
                      ])),
            ],
          ),
        ));
  }
}
