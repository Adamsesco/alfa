import 'package:alpha/app/services/categories_services.dart';
import 'package:alpha/models/entity/category.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({Key key}) : super(key: key);

  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  List<Category> cats = new List<Category>();
  List<Category> cats_areas = new List<Category>();

  CategoriesService cat = new CategoriesService();
  bool load = true;

  getList() async {
    var a = await cat.get_all_governorates();
    if (!this.mounted) return;
    setState(() {
      cats = a;
      load = false;
    });
  }

  getareas(id) async {
    var a = await cat.get_areas(id);
    if (!this.mounted) return;
    setState(() {
      cats_areas = a;

      print("--------------------");
      print(cats_areas);
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
  }

  @override
  Widget build(BuildContext context) {
    List map = [
      {"name": "Governorate", "children": cats},
      {"name": "Area", "children": cats_areas},
      {"name": "Product", "children": []},
      {"name": "Service", "children": []}
    ];

    Widget button(String title, Color color, String number) => Container(
        width: 102.w,
        child: RaisedButton(
            padding: EdgeInsets.all(5.w),
            elevation: 0,
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(2),
              ),
            ),
            color: color,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: TextStyle(
                      height: 1.2,
                      color: Colors.white,
                      fontFamily: context.locale == Locale('ar', 'AR')
                          ? "cairo bold"
                          : "rubik medium",
                      fontWeight: FontWeight.w500,
                      fontSize: 15.0.sp),
                ),
                Expanded(child: Container(width: 8.w)),
                Padding(
                    padding: EdgeInsets.all(2),
                    child: CircleAvatar(
                      radius: 14.r,
                      backgroundColor: Colors.white,
                      child: Center(
                          child: Text(number,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 13.5.sp,
                                  fontFamily:
                                      context.locale == Locale('ar', 'AR')
                                          ? "cairo bold"
                                          : "rubik medium"))),
                    ))
              ],
            ),
            onPressed: () {}));
    List<Widget> list_product = [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          button("Ultra Super", ColorsApp.col_green_f, "98"),
          Container(
            width: 5.w,
          ),
          button("Super", ColorsApp.col_app_brown_f, "95"),
          Container(
            width: 5.w,
          ),
          button("Premium", ColorsApp.col_red, "91")
        ],
      ),
    ];

    Widget border_widget2(String icon, Color color) => Container(
        /* decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(12.5.r)),*/
        padding: EdgeInsets.all(11.w),
        child: SvgPicture.asset(
          icon,
        ));
    List<Widget> list_services = [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          border_widget2("assets/images/ba9ala.svg", ColorsApp.col_green),
          Container(
            width: 8.w,
          ),
          border_widget2("assets/images/subway.png", ColorsApp.col_black),
        ],
      )
    ];

    return Scaffold(
        bottomNavigationBar: Container(
            padding: EdgeInsets.all(12.h),
            height: 74.h,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  width: 140.w,
                  child: FlatButton(
                      onPressed: () {},
                      child: Text(
                        "CLEAR".tr().toUpperCase(),
                        style: TextStyle(
                            fontSize: 16.sp, color: ColorsApp.col_grey_text),
                      ))),
              Container(
                width: 24.w,
              ),
              Container(
                  width: 140.w,
                  child: PrimaryButton(
                      text: "apply_filter".tr().toUpperCase(),
                      color_text: ColorsApp.col_app,
                      color: ColorsApp.col_app,
                      onTap: () {})),
            ])),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: Container(),
          title: Text(
            "FILTER",
            style: TextStyle(
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
                color: ColorsApp.col_black),
          ),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: SvgPicture.asset("assets/icons/close.svg"))
          ],
        ),
        body: ListView(
            children: map
                .map((e) => new ExpansionTile(
                    collapsedIconColor: Colors.transparent,
                    iconColor: Colors.transparent,
                    title: Center(
                      child: new Text(
                        e["name"].toString().toUpperCase(),
                        style: TextStyle(fontWeight: FontWeight.w800),
                      ),
                    ),
                    backgroundColor:
                        Theme.of(context).accentColor.withOpacity(0.025),
                    children: e["name"] == "Product"
                        ? list_product
                        : e["name"] == "Service"
                            ? list_services
                            : List<Widget>.from(e["children"]
                                .map((a) => new ListTile(
                                      title: Text((e["name"] == "Governorate" ||
                                              e["name"] == "Area")
                                          ? a.name
                                          : a),
                                      onTap: () {
                                        getareas(a.id);
                                      },
                                    ))
                                .toList())))
                .toList()));
  }
}
