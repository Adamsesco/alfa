import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/views/dashboard/dashboard_screen.dart';
import 'package:alpha/presentation/views/dashboard/my_cards.dart';
import 'package:alpha/presentation/views/map_bview/map_view.dart';
import 'package:alpha/presentation/views/notif_screen/notifications_screen.dart';
import 'package:alpha/presentation/views/recharge_screen/recharge_screen.dart';
import 'package:alpha/presentation/views/services/services_sccreen.dart';
import 'package:alpha/presentation/views/settings/drawer_view.dart';
import 'package:alpha/presentation/views/signin/signin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:alpha/presentation/views/home/home_screen.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class BottomNavigationScreen extends StatefulWidget {
  @override
  _BottomNavigationScreenState createState() => _BottomNavigationScreenState();
}

class _BottomNavigationScreenState extends State<BottomNavigationScreen> {
  int _currentIndex = 0;
  PageController _pageController = PageController();
  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key
  bool bl_notif = false;

  @override
  void initState() {
    super.initState();
  }

  please_login() {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Text('Please login to conitnue.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return SignInScreen();
              }));
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);

    // final BottomNavigationBarBloc bottomNavigationBloc = BlocProvider.of<BottomNavigationBarBloc>(context);
    List items = [
      {
        "name": "home".tr(),
        "color_active": ColorsApp.col_app,
        "color_innactive": ColorsApp.col_other_grey,
        "icon": "assets/icons/home.svg"
      },

      {
        "name": "dashboard".tr(),
        "color_active": ColorsApp.col_app,
        "color_innactive": ColorsApp.col_other_grey,
        "icon": "assets/icons/dashboard.svg"
      },

      {
        "name": "racharge".tr().toUpperCase(),
        "color_active": ColorsApp.col_app,
        "color_innactive": ColorsApp.col_other_grey,
        "icon": "assets/icons/Soustraction.svg"
      },
      {
        "name": "locations".tr(),
        "color_active": ColorsApp.col_app,
        "color_innactive": ColorsApp.col_other_grey,
        "icon": "assets/icons/location.svg"
      },
     /* {
        "name": "services".tr(),
        "color_active": ColorsApp.col_app,
        "color_innactive": ColorsApp.col_other_grey,
        "icon": "assets/icons/services.svg"
      },*/

    ];

    return Scaffold(
      drawer: DrawerView(),
      key: _key,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Padding(
            padding: EdgeInsets.all(12),
            child: SvgPicture.asset(
              "assets/images/logo.svg",
              width: 120.w,
            )),
        elevation: 0,
        leading: DrawerButton(
          onPressed: () {
            _key.currentState.openDrawer();
          },
        ),
        actions: [
          IconButton(
            onPressed: () {
              if (myProvider.isAuthenticate == false)
                please_login();
              else {
                if (_currentIndex != 0) {
                  _pageController.animateToPage(0,
                      duration: Duration(milliseconds: 100),
                      curve: Curves.ease);
                }
                if (bl_notif == false)
                  setState(() {
                    bl_notif = true;
                  });
                else
                  setState(() {
                    bl_notif = false;
                  });
              }
            },
            icon: SvgPicture.asset(
              "assets/icons/notif.svg",
              color: bl_notif == false
                  ? ColorsApp.col_other_grey
                  : ColorsApp.col_app,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset("assets/icons/wb.svg"),
          ),
        ],
      ),
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: (index) {
          if (!this.mounted) return;
          setState(() => _currentIndex = index);
        },
        children: <Widget>[
          bl_notif == true ? NotificationScreen() : HomeScreen(),
          MyCardsScreen(),
          RechargeScreen(),
          MapScreeen(),
        //  ServicesScreen(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          // use this to remove appBar's elevation
          onTap: (index) => setState(() {
                _currentIndex = index;

                bl_notif = false;
                _pageController.animateToPage(index,
                    duration: Duration(milliseconds: 100), curve: Curves.ease);
              }),
          items: items
              .map((e) => BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      e["icon"],
                      color: _currentIndex == items.indexOf(e)
                          ? e["color_active"]
                          : e["color_innactive"],
                    ),
                    title: Padding(
                        padding: EdgeInsets.only(top: 10.h, bottom: 10.h),
                        child: Text(
                          e["name"],
                          style: TextStyle(
                            fontWeight: _currentIndex == items.indexOf(e)
                                ? FontWeight.w600
                                : FontWeight.normal,
                            color: _currentIndex == items.indexOf(e)
                                ? e["color_active"]
                                : e["color_innactive"],
                          ),
                        )),
                  ))
              .toList()),
    );
  }
}

/***
    BlocBuilder<BottomNavigationBarEvent, BottomNavigationBarState>(
    bloc: bottomNavigationBloc,
    builder: (BuildContext context, BottomNavigationBarState state){
    /*if (state is LoadHome) {
    return Center(child: CircularProgressIndicator());
    }*/
    if (state is LoadHome) {
    return HomeScreen();
    }
    if (state is LoadDashboard) {
    return DashBoardScreen();
    }
    return Container();
    },
    ),)
 **/
