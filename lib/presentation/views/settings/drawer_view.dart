import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/settings/widget_lang.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    logout() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      String id = prefs.getString("id");

      var myProvider = Provider.of<LoginProvider>(context, listen: false);

      if (myProvider.isAuthenticate) {
        myProvider.mitexto = 'Provider';

        myProvider.isAuthenticate = false;
        //StorageUtil.putBool('isAutenticado', null);
        prefs.clear();
        Navigator.pop(context);
      }
    }

    Widget row(String name, String icon, onPressed) => InkWell(
        onTap: () {
          onPressed();
        },
        child: Row(
          children: [
            Container(width: 42.w),
            SvgPicture.asset(
              "assets/icons/" + icon,
              color: ColorsApp.col_app,
            ),
            Container(
              width: 10.w,
            ),
            Text(
              name.toString(),
              style: TextStyle(
                  color: ColorsApp.col_app,
                  fontWeight: FontWeight.bold,
                  height: 1.2,
                  fontSize: 20.sp),
            ),
            Container(
              width: 20,
            ),
            icon == "language.svg" ? WidgetLangue() : Container()
          ],
        ));

    return Container(
        width: 299.w,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(color: Colors.white)
        /* gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.3, 1],
                colors: [ColorsApp.col_app_l, ColorsApp.col_app_f]))*/
        ,
        child: ListView(
          children: [
            Container(height: 40.h),
            Row(
              children: [
                Container(
                  width: 33.w,
                ),
                SvgPicture.asset("assets/images/lg.svg"),
                Expanded(
                  child: Container(),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: SvgPicture.asset(
                      context.locale == Locale('ar', 'AR')
                          ? "assets/icons/arrow_a.svg"
                          : "assets/icons/aarrow1.svg",
                      color: ColorsApp.col_app,
                    )),
                Container(
                  width: 12.w,
                ),
              ],
            ),
            Container(
              height: 48.h,
            ),
            Row(
              children: [
                Container(width: 28.w),
                Container(
                  height: ScreenUtil().setWidth(70),
                  width: ScreenUtil().setWidth(70),
                  child: ClipOval(
                    child: Image.asset(
                      "assets/images/pp.png",
                      height: ScreenUtil().setWidth(70),
                      width: ScreenUtil().setWidth(70),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(width: 12.w),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "M. Ahmed Salem",
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                    Container(
                      height: 4.h,
                    ),
                    Text("Member since 2019")
                  ],
                )
              ],
            ),
            Container(
              height: 42.h,
            ),
            Row(children: [
              Container(
                padding: EdgeInsets.all(12.h),
                decoration: BoxDecoration(
                  color: ColorsApp.col_white_grey,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(30.r),
                      topRight: Radius.circular(30.r)),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 24.w,
                    ),
                    SvgPicture.asset("assets/icons/wallet.svg"),
                    Container(
                      width: 24.w,
                    ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Wallet balance",
                            style: TextStyle(
                                fontSize: 14.sp, fontWeight: FontWeight.w300),
                          ),
                          Container(
                            height: 2.h,
                          ),
                          Row(
                            children: [
                              Text(
                                "65.800",
                                style: TextStyle(
                                    fontSize: 15.sp,
                                    fontFamily:
                                        context.locale == Locale('ar', 'AR')
                                            ? "cairo bold"
                                            : "rubik medium",
                                    fontWeight: FontWeight.w700),
                              ),
                              Container(
                                width: 4.w,
                              ),
                              Text(
                                "KWD",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 14.sp),
                              ),
                            ],
                          )
                        ]),
                    Container(
                      width: 28.w,
                    ),
                  ],
                ),
              )
            ]),
            Container(
              height: 42.h,
            ),
            row("about".tr().toUpperCase(), "info.svg", () {}),
            Container(
              height: 16.h,
            ),
            row("language".tr().toUpperCase(), "language.svg", () {}),
            Container(
              height: 16.h,
            ),
            row("faq".tr().toUpperCase(), "question.svg", () {}),
            Container(
              height: 26.h,
            ),
            row("contact_us".tr().toUpperCase(), "Soustraction 2.svg", () {}),
            Container(
              height: 80.h,
            ),
            row("logout".tr().toUpperCase(), "logout.svg", logout),
            Container(
              height: 42.h,
            ),
            Row(children: [
              Container(
                width: 33.w,
              ),
              Text(
                "privacy".tr(),
                style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w400,
                    height: 1.2,
                    color: ColorsApp.col_app),
              )
            ]),
            Container(
              height: 8.h,
            ),
            Row(children: [
              Container(
                width: 33.w,
              ),
              Text(
                "terms".tr(),
                style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w400,
                    height: 1.2,
                    color: ColorsApp.col_app),
              ),
            ]),
            Container(
              height: 28.h,
            ),
            Row(children: [
              Container(
                width: 33.w,
              ),
              Text(
                "right".tr(),
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12.sp,
                    height: 1.2,
                    color: ColorsApp.col_app),
              ),
            ]),
          ],
        ));
  }
}
