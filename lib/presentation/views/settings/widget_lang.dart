import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class WidgetLangue extends StatefulWidget {
  @override
  _WidgetLangueState createState() => _WidgetLangueState();
}

class _WidgetLangueState extends State<WidgetLangue> {
  @override
  Widget build(BuildContext context) {

    print(context.locale);
    return context.locale == Locale('ar', 'AR')
        ? FlatButton(
            onPressed: () {
              context.locale = Locale('en', 'US');
            },
            child: Text("English"))
        : FlatButton(
            onPressed: () {
              context.locale = Locale('ar', 'AR');
            },
            child: Text("Arabic"));
  }
}
