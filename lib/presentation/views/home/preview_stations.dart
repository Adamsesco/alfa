import 'package:alpha/models/entity/station.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PreviwStation extends StatefulWidget {
  PreviwStation(this.products);

  List<Station> products;

  @override
  _FeaturedItemsState createState() => _FeaturedItemsState();
}

class _FeaturedItemsState extends State<PreviwStation> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: ScreenUtil().setHeight(210),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children:
              widget.products.map((Station e) => FeaturedItem(e)).toList(),
        ));
  }
}

class FeaturedItem extends StatelessWidget {
  FeaturedItem(this.station);

  Station station;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            width: ScreenUtil().setWidth(216),
            height: ScreenUtil().setHeight(128),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                    //<--clipping image

                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/images/placeholder.jpeg"),
                                fit: BoxFit.contain)),
                        child: station.images == null
                            ? Container()
                            : Hero(
                                tag: station.images,
                                child: FadeInImage.assetNetwork(
                                  image: station.images[0],
                                  placeholder: "assets/images/placeholder.jpeg",
                                  width: ScreenUtil().setWidth(216),
                                  height: ScreenUtil().setHeight(128),
                                  fit: BoxFit.cover,
                                )))),

                SizedBox(
                  height: 8,
                ),

                Container(
                    // height: ScreenUtil().setHeight(54),
                    width: ScreenUtil().setWidth(110),
                    child: new RichText(
                        text: new TextSpan(
                            text: station.name.split(' ')[0].toUpperCase()+ " ",
                            style: new TextStyle(
                                color: ColorsApp.col_app,
                                fontWeight: FontWeight.w800,
                                fontSize: ScreenUtil().setSp(14)),
                            children: <TextSpan>[
                          new TextSpan(
                              text: station.name.split(' ')[1].toUpperCase(),
                              style: new TextStyle(
                                  color: ColorsApp.col_app,
                                  fontWeight: FontWeight.w400,
                                  fontSize: ScreenUtil().setSp(13))),
                        ]))),
                SizedBox(
                  height: 4,
                ),
                Container(
                    height: ScreenUtil().setHeight(34),
                    width: ScreenUtil().setWidth(190),
                    child: Text(
                      station.description.toString(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.6,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                // Expanded(child: Container(),),
              ],
            )),
        onTap: () {
          /* Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
                return DetailsProduct(product);
              }));*/
        });
  }
}
