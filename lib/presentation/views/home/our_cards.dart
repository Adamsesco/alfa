import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OurCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget wid = Container(
        height: 246.h,
        child: new Swiper(
          containerHeight: 246.h,
          itemHeight: 246.h,
          containerWidth: MediaQuery.of(context).size.width*0.9,
          itemBuilder: (BuildContext context, int index) {
            return ClipRRect(
                //<--clipping image

                borderRadius: BorderRadius.all(Radius.circular(16.r)),
            child: new Image.asset(
              "assets/images/back1.png",
              fit: BoxFit.cover,
              width: MediaQuery.of(context).size.width*0.9,
              height: 296.h,
            ));
          },
          itemCount: 3,
          viewportFraction: 0.8,
          scale: 0.9,
        ));
    return wid;
  }
}
