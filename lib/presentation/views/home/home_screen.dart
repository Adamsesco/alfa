import 'package:alpha/models/entity/fuel.dart';
import 'package:alpha/models/entity/station.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/home/our_cards.dart';
import 'package:alpha/presentation/views/home/preview_stations.dart';
import 'package:alpha/presentation/views/items/fuel_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Widget cards_widget = Container();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          height: 12.h,
        ),
        PreviwStation(Station.stations_items),
        Divider(),
        Container(
          height: 10.h,
        ),
       // CustomTexts.TextWidget0("our_cards".tr().toUpperCase(), context.locale),
        Container(
          height: 12.h,
        ),
        OurCards(),
        Container(
          height: 18.h,
        ),
        CustomTexts.TextWidget("fuel_price".tr().toUpperCase(), context.locale),
        Container(
          height: 10.h,
        ),
        Container(
          height: 12.h,
        ),
        Container(
            height: 200.h,
            child: ListView(
                physics: const NeverScrollableScrollPhysics(),
                children: Fuel.list.map((e) => FuelCard(e)).toList()))
      ],
    );
  }
}
