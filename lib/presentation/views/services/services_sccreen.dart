import 'package:alpha/models/entity/station.dart';
import 'package:alpha/presentation/custom_widgets/dotted_slider.dart';
import 'package:alpha/presentation/views/items/service_card_info.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class ServicesScreen extends StatefulWidget {
  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  List<String> images = [
    "assets/images/back1.png",
    "assets/images/back2.png",
    "assets/images/back3.png"
  ];

  @override
  Widget build(BuildContext context) {

    TextWidget(String name,ctx) {
      return Padding(
          padding: EdgeInsets.only(left: 27.w),
          child: new RichText(
              text: new TextSpan(
                  text: name.split(' ')[0].toUpperCase() + " ",
                  style: new TextStyle(
                      color: ColorsApp.col_black,
                      fontFamily: ctx==Locale('ar', 'AR')?"cairo bold":"rubik medium",
                      fontWeight: FontWeight.w800,
                      fontSize: ScreenUtil().setSp(19)),
                  children: <TextSpan>[
                    new TextSpan(
                        text: name.split(' ')[1].toUpperCase(),
                        style: new TextStyle(
                            color: ColorsApp.col_black,
                            fontWeight: FontWeight.w400,
                            fontSize: ScreenUtil().setSp(19))),
                  ])));
    }

    Widget card = DottedSlider(
        color: Colors.white,
        maxHeight: ScreenUtil().setHeight(169),
        children: List<String>.from(images)
            .map<Widget>((String im) => Container(
                child: ClipRRect(
                    //<--clipping image
                    borderRadius: BorderRadius.all(Radius.circular(16.r)),
                    child: Image.asset(
                      im,
                      width: MediaQuery.of(context).size.width,
                      height: ScreenUtil().setHeight(169),
                      fit: BoxFit.cover,
                    ))))
            .toList());

    return ListView(
      padding: EdgeInsets.only(top:16.w,left: 16.w,right: 16.w),
      children: [
        card,
        Container(height:
        16.h,),
        TextWidget("online_services".tr().toUpperCase(),context.locale),
        Container(height:
          16.h,),
        Container(
          height: 410.h,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: Station.stations_items
                .map((e) => Padding(
                    padding: EdgeInsets.only(right: 12.w),
                    child: SeerviceItem(e)))
                .toList(),
          ),
        ),
        Container(height:
        16.h,),
      ],
    );
  }
}
