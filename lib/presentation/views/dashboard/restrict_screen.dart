import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/dashboard/stations_screens.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class RestrictScreen extends StatefulWidget {
  @override
  _RestrictScreenState createState() => _RestrictScreenState();
}

class _RestrictScreenState extends State<RestrictScreen> {
  @override
  Widget build(BuildContext context) {
    Widget widget_item(String text, Color color, String icon) => Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: ColorsApp.col_gr_back, //color of shadow
              spreadRadius: 5, //spread radius
              blurRadius: 7, // blur radius
              offset: Offset(0, 2), // changes position of shadow
            ),
            //you can set more BoxShadow() here
          ], color: Colors.white, borderRadius: BorderRadius.circular(12.5.r)),
          padding: EdgeInsets.only(top: 22.h, bottom: 22.h),
          child: Row(
            children: [
              Container(
                width: 36.w,
              ),
              icon == "assets/icons/add.png"
                  ? Image.asset(
                      "assets/icons/plus.png",
                      color: ColorsApp.col_app,
                    )
                  : SvgPicture.asset(
                      icon,
                      color: ColorsApp.col_app,
                    ),
              Container(width: 30.w),
              Text(
                text.toUpperCase(),
                style: TextStyle(
                    height: 1.2,
                    color: ColorsApp.col_app,
                    fontFamily: context.locale == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",
                    fontWeight: FontWeight.w400,
                    fontSize: 15.5.sp),
              )
            ],
          ),
        );

    Widget button(String text, Color color, Color color_text) => RaisedButton(
        elevation: 0,
        shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        color: color,
        child: Text(
          text,
          style: TextStyle(
              height: 1.2,
              color: color_text,
              fontFamily: context.locale == Locale('ar', 'AR')
                  ? "cairo bold"
                  : "rubik medium",
              fontWeight: FontWeight.w500,
              fontSize: 13.5.sp),
        ),
        onPressed: () {});

    Widget button_arrow(String text, Color color, Color color_text) =>
        RaisedButton(
            elevation: 0,
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            color: color,
            child: Row(
              children: [
                Text(
                  text,
                  style: TextStyle(
                      color: color_text,
                      fontFamily: context.locale == Locale('ar', 'AR')
                          ? "cairo bold"
                          : "rubik medium",
                      fontWeight: FontWeight.w500,
                      fontSize: 13.5.sp),
                ),
                Container(
                  width: 12.w,
                ),
                SvgPicture.asset(
                  "assets/icons/arr.svg",
                  width: 11.w,
                )
              ],
            ),
            onPressed: () {});

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Padding(
                padding: EdgeInsets.all(16),
                child: SvgPicture.asset(
                  "assets/images/logo.svg",
                  width: 110.w,
                )),
            elevation: 0,
            leading: ArrowButton(),
            actions: [
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset(
                  "assets/icons/notif.svg",
                  color: ColorsApp.col_app,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("assets/icons/wb.svg"),
              ),
            ]),
        body: Container(
            padding: EdgeInsets.all(16.w),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                  padding: EdgeInsets.only(left: 0.w),
                  child: CustomTexts.TextWidget3(
                      //Cost & purchase Limits
                      'restricted_by'.tr().toUpperCase(),
                      'restricted_type'.tr().toUpperCase(),
                      context.locale)),
              Container(
                height: 16.h,
              ),
              Row(
                children: [
                  button("ULTRA SUPER", ColorsApp.col_green, Colors.white),
                  Container(
                    width: 10.w,
                  ),
                  button("SUPER", ColorsApp.col_app_brown, Colors.white),
                  Container(
                    width: 10.w,
                  ),
                  button("PREMIUM", ColorsApp.col_red, Colors.white),
                ],
              ),
              Row(
                children: [
                  button("diesel".toUpperCase(), ColorsApp.col_gr_back,
                      ColorsApp.col_grey_clear),
                  Container(
                    width: 10.h,
                  ),
                  button("Kerosene".toUpperCase(), ColorsApp.col_gr_back,
                      ColorsApp.col_grey_clear),
                ],
              ),
              Container(
                height: 10.h,
              ),
              Container(
                  padding: EdgeInsets.only(left: 0.w),
                  child: CustomTexts.TextWidget3(
                      //Cost & purchase Limits
                      'cost_purchase1'.tr().toUpperCase(),
                      'cost_purchase'.tr().toUpperCase(),
                      context.locale)),
              Container(
                height: 16.h,
              ),
              Row(
                children: [
                  Container(
                      width: 102.w,
                      child: button("Daily".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_clear)),
                  Container(
                    width: 10.h,
                  ),
                  Container(
                      width: 102.w,
                      child: button("25 Kd".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_text)),
                  Container(
                    width: 10.h,
                  ),
                  Container(
                      width: 102.w,
                      child: button("2 PPD".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_text)),
                ],
              ),
              Row(
                children: [
                  Container(
                      width: 102.w,
                      child: button("Weekly".toUpperCase(), ColorsApp.col_app,
                          Colors.white)),
                  Container(
                    width: 10.h,
                  ),
                  Container(
                      width: 102.w,
                      child: button_arrow("175 KD".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_text)),
                  Container(
                    width: 10.h,
                  ),
                  Container(
                      width: 102.w,
                      child: button_arrow("14 PPD".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_text)),
                ],
              ),
              Row(
                children: [
                  Container(
                      width: 102.w,
                      child: button("Monthly".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_clear)),
                  Container(
                    width: 10.h,
                  ),
                  Container(
                      width: 102.w,
                      child: button("700 KD".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_text)),
                  Container(
                    width: 10.h,
                  ),
                  Container(
                      width: 102.w,
                      child: button("57 PPD".toUpperCase(),
                          ColorsApp.col_gr_back, ColorsApp.col_grey_text)),
                ],
              ),
              Container(
                height: 30.h,
              ),
              Container(
                  padding: EdgeInsets.only(left: 0.w),
                  child: CustomTexts.TextWidget3(
                      //Cost & purchase Limits
                      'restricted_by'.tr().toUpperCase(),
                      'restricted_station'.tr().toUpperCase(),
                      context.locale)),
              Container(
                height: 12.h,
              ),
              InkWell(
                child: widget_item("Add new restriction to station",
                    ColorsApp.col_grey_clear, "assets/icons/add.png"),
                onTap: () {
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (BuildContext context) {
                    return StationsPage();
                  }));
                },
              ),
              Container(
                height: 12.h,
              ),
              widget_item("Alfa Petrol Station Sabahiya", ColorsApp.col_app,
                  "assets/icons/fu.svg"),
              Container(
                height: 16.h,
              ),
              widget_item("Alfa Petrol Station AL BIDAA", ColorsApp.col_app,
                  "assets/icons/fu.svg")
            ])));
  }
}
