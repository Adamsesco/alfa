import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/app/services/products_services.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/custom_textfield.dart';
import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/dashboard/restrict_screen.dart';
import 'package:alpha/presentation/views/signin/signin.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class AddCardScreen extends StatefulWidget {
  @override
  _AddCardScreenState createState() => _AddCardScreenState();
}

class _AddCardScreenState extends State<AddCardScreen> {
  final _ccontroller = new TextEditingController();
  final _namecontroller = new TextEditingController();
  FocusNode _focusname = new FocusNode();
  FocusNode _focusconfirm = new FocusNode();
  bool check = false;
  String selected = "";
  bool show = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List items = [
    {
      "icon": "assets/icons/selec1.svg",
      "icon_active": "assets/icons/alpha_pay.svg",
      "check": false,
      "name": "Alpha Pay"
    },
    {
      "icon": "assets/icons/lg_tap.svg",
      "icon_active": "assets/icons/lg_tap.svg",
      "check": false,
      "name": "Alpha Tap"
    },
    {
      "icon": "assets/icons/lg_item.svg",
      "icon_active": "assets/icons/lg_tap.svg",
      "check": false,
      "name": "Individual"
    },
    {
      "icon": "assets/icons/id_card.svg",
      "icon_active": "assets/icons/selec2.svg",
      "check": false,
      "name": "Civil ID"
    },
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
      backgroundColor: Colors.red[900],
    ));
  }

  Widget widget_card2(index,
          {String icon,
          Color color_text,
          String name,
          icon_active,
          String code,
          bool check,
          Color color,
          String price}) =>
      GestureDetector(
          onTap: () {
            print(check);

            if (items[index]["check"] == false) {
              print("hhdhdhdh");

              setState(() {
                for (var i in items) {
                  setState(() {
                    i["check"] = false;
                  });
                }
                items[index]["check"] = true;
                color_text = Colors.white;
                color = ColorsApp.col_app;
              });
            }
          },
          child: Container(
            width: 150.w,
            height: 150.w,
            padding: EdgeInsets.all(16.w),
            decoration: BoxDecoration(
              color: items[index]["check"] == false
                  ? Colors.white
                  : ColorsApp.col_app,
              boxShadow: [
                BoxShadow(
                  color: ColorsApp.col_gr_back, //color of shadow
                  spreadRadius: 5, //spread radius
                  blurRadius: 7, // blur radius
                  offset: Offset(0, 2), // changes position of shadow
                ),
                //you can set more BoxShadow() here
              ],
              borderRadius: BorderRadius.all(Radius.circular(16.r)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                    items[index]["check"] == false ? icon : icon_active),
                Container(
                  height: 24.h,
                ),
                Text(
                  name,
                  style: TextStyle(
                      height: 1.2,
                      color: color_text,
                      fontWeight: FontWeight.w800),
                ),
                Container(
                  height: 8.h,
                ),
              ],
            ),
          ));

  please_login() {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Text('Please login to conitnue.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return SignInScreen();
              }));
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);

    Widget list_widgets = Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            widget_card2(0,
                icon_active: items[0]["icon_active"],
                check: items[0]["check"],
                icon: items[0]["icon"],
                name: items[0]["name"],
                color: Colors.white),
            Container(
              height: 12.w,
            ),
            widget_card2(1,
                icon_active: items[1]["icon_active"],
                check: items[1]["check"],
                icon: items[1]["icon"],
                name: items[1]["name"],
                color: Colors.white)
          ],
        ),
        Container(
          width: 12.w,
        ),
        Column(
          children: [
            widget_card2(2,
                icon_active: items[2]["icon_active"],
                check: items[2]["check"],
                icon: items[2]["icon"],
                name: items[2]["name"],
                color: Colors.white),
            Container(
              height: 12.w,
            ),
            widget_card2(3,
                icon_active: items[3]["icon_active"],
                check: items[3]["check"],
                icon: items[3]["icon"],
                name: items[3]["name"],
                color: Colors.white)
          ],
        )
      ],
    );

    /**
        GridView.builder(
        itemCount: items.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1.2,
        ),
        itemBuilder: (context, i) {
        final e = items[i];
        return widget_card2(
        icon: e["icon"], name: e["name"], color: Colors.white);
        }))
     */

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Padding(
              padding: EdgeInsets.all(12),
              child: SvgPicture.asset(
                "assets/images/logo.svg",
                width: 120.w,
              )),
          elevation: 0,
          leading: ArrowButton(),
          actions: [
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(
                "assets/icons/notif.svg",
                color: ColorsApp.col_app,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset("assets/icons/wb.svg"),
            ),
          ]),
      body: Container(
        padding: EdgeInsets.all(16.w),
        child: ListView(
          padding: EdgeInsets.all(12.w),
          children: [
            Container(
                padding: EdgeInsets.only(left: 16.w),
                child: CustomTexts.TextWidget3(
                    "add_new".tr().split(" ")[0] +
                        " " +
                        "add_new".tr().split(" ")[1],
                    "add_new".tr().split(" ")[2],
                    context.locale)),
            Container(
              height: 20.h,
            ),
            list_widgets,
            Container(
              height: 12.h,
            ),
            Container(
                //  padding: EdgeInsets.only(left: 34.w, right: 34.w),
                child: Theme(
                    data: ThemeData(
                      hintColor: ColorsApp.col_gr_back,
                    ),
                    child: TextFieldWidget(
                      _namecontroller,
                      _focusname,
                      "DRIVER",
                      false,
                      wid: Container(),
                    ))),
            Container(height: 12.h),
            Container(
                //  padding: EdgeInsets.only(left: 34.w, right: 34.w),
                child: Theme(
                    data: ThemeData(
                      hintColor: ColorsApp.col_gr_back,
                    ),
                    child: TextFieldWidget(
                      _ccontroller,
                      _focusconfirm,
                      "code".tr(),
                      false,
                      wid: Container(),
                    ))),
            Container(
              height: 12.h,
            ),
            Center(
                child: SizedBox(
                    height: 54.h,
                    width: 189.w,
                    child: PrimaryButton(
                        isLoading: show,
                        text: "create_card".tr(),
                        color_text: ColorsApp.col_app,
                        color: ColorsApp.col_app,
                        onTap: () async {
                          if (myProvider.isAuthenticate == false)
                            please_login();
                          else {
                            setState(() {
                              show = true;
                            });
                            var a =
                                await ProductSServices.createCivilIDCustomer({
                              "email": "email@ksks.com",
                              "address": "kuwait",
                              "area_id": 1,
                              "civil_id": "288081102429",
                              "contact_tel": "22478858",
                              "first_name": "Fadi",
                              "gov_id": 1,
                              "last_name": "Muhtadi",
                              "mobile": "22478868",
                              "po_box": "11001",
                              "car_serial": "9", //serial
                              "title": "Mr.",
                              "zip_code": "35151"
                            });

                            print("-------------------------------");
                            print(a);

                            setState(() {
                              show = false;
                            });

                            if (a["result"] == "false") {
                              showInSnackBar(a["detail"]);
                            } else
                              Navigator.push(context, new MaterialPageRoute(
                                  builder: (BuildContext context) {
                                return RestrictScreen();
                              }));
                          }
                        }))),
          ],
        ),
      ),
    );
  }
}
