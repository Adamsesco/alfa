import 'package:alpha/presentation/views/dashboard/add_crad_screen.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:easy_localization/easy_localization.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  @override
  Widget build(BuildContext context) {
    TextWidget1(String name) {
      return Padding(
          padding: EdgeInsets.only(left: 27.w),
          child: new RichText(
              text: new TextSpan(
                  text: name.split(' ')[0].toUpperCase() + " ",
                  style: new TextStyle(
                      color: ColorsApp.col_black,
                      fontWeight: FontWeight.w800,
                      fontFamily: context.locale == Locale('ar', 'AR')
                          ? "cairo bold"
                          : "rubik medium",
                      fontSize: ScreenUtil().setSp(17)),
                  children: <TextSpan>[
                new TextSpan(
                    text: name.split(' ')[1].toUpperCase(),
                    style: new TextStyle(
                        color: ColorsApp.col_black,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(17))),
              ])));
    }

    TextWidget3(String name1, String name2) {
      return Padding(
          padding: EdgeInsets.only(left: 0.w),
          child: new RichText(
              text: new TextSpan(
                  text: name1 + " ",
                  style: new TextStyle(
                      color: ColorsApp.col_black,
                      fontWeight: FontWeight.w800,
                      fontFamily: context.locale == Locale('ar', 'AR')
                          ? "cairo bold"
                          : "rubik medium",
                      fontSize: ScreenUtil().setSp(17)),
                  children: <TextSpan>[
                new TextSpan(
                    text: name2,
                    style: new TextStyle(
                        color: ColorsApp.col_black,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(17))),
              ])));
    }

    TextWidget(String name) {
      return Padding(
          padding: EdgeInsets.only(left: 27.w),
          child: new RichText(
              text: new TextSpan(
                  text: name.split(' ')[0].toUpperCase() +
                      " " +
                      name.split(' ')[1].toUpperCase() +
                      " ",
                  style: new TextStyle(
                      color: ColorsApp.col_black,
                      fontWeight: FontWeight.w800,
                      fontFamily:
                          context.locale == Locale('ar', 'AR') ? "cairo bold" : "rubik medium",
                      fontSize: ScreenUtil().setSp(17)),
                  children: <TextSpan>[
                new TextSpan(
                    text: name.split(' ')[2].toUpperCase(),
                    style: new TextStyle(
                        color: ColorsApp.col_black,
                        fontWeight: FontWeight.w200,
                        fontFamily:
                        context.locale == Locale('ar', 'AR') ? "cairo bold" : "rubik",
                        fontSize: ScreenUtil().setSp(17))),
              ])));
    }

    Widget wid = Container(
        height: 200.h,
        child: new Swiper(
          containerHeight: 200.h,
          itemHeight: 206.h,
          containerWidth: MediaQuery.of(context).size.width * 0.9,
          itemBuilder: (BuildContext context, int index) {
            return ClipRRect(
                //<--clipping image

                borderRadius: BorderRadius.all(Radius.circular(16.r)),
                child: new Image.asset(
                  "assets/images/card.png",
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 296.h,
                ));
          },
          itemCount: 3,
          viewportFraction: 0.8,
          scale: 0.9,
        ));

    return ListView(
      padding: EdgeInsets.only(top: 16.w, left: 16.w, right: 16.w),
      children: [
        Container(
          height: 16.h,
        ),
        TextWidget1("manage_cards".tr().toUpperCase()),
        Container(height: 21.h),
        Container(
            width: 373.w,
            height: 246.h,
            child: Stack(children: [
              ClipRRect(
                  //<--clipping image

                  borderRadius: BorderRadius.all(Radius.circular(16.r)),
                  child: Container(
                      color: Colors.grey[100],
                      child: new Image.asset(
                        "assets/images/alfa_card.png",
                        fit: BoxFit.cover,
                        width: 373.w,
                        height: 246.h,
                      ))),
              Align(
                alignment: Alignment.center,
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return AddCardScreen();
                    }));
                  },
                  child: TextWidget3(
                      "add_new".tr().split(" ")[0] +
                          " " +
                          "add_new".tr().split(" ")[1],
                      "add_new".tr().split(" ")[2]),
                ),
              )
            ])),
        Container(
          height: 34.h,
        ),
        TextWidget("edit_card".tr().toUpperCase()),
        Container(
          height: 9.h,
        ),
        Padding(
            padding: EdgeInsets.only(left: 27.w),
            child: Container(
                child: Text(
              "lorem".tr(),
              style: TextStyle(
                  fontSize: 13.8.sp,
                  color: const Color(0xff807F82)),
            ))),
        Container(
          height: 34.h,
        ),
        wid,
        Container(
          height: 20.h,
        ),
      ],
    );
  }
}
