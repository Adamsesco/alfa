import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/app/services/products_services.dart';
import 'package:alpha/models/entity/card_model.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/dashboard/dashboard_screen.dart';
import 'package:alpha/presentation/views/signin/signin.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import 'add_crad_screen.dart';

class MyCardsScreen extends StatefulWidget {
  const MyCardsScreen({Key key}) : super(key: key);

  @override
  _MyCardsScreenState createState() => _MyCardsScreenState();
}

class _MyCardsScreenState extends State<MyCardsScreen> {
  List<CardModel> cards = [];
  bool show = true;

  please_login() {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Text('Please login to conitnue.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return SignInScreen();
              }));
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

  get_mycards() async {
    var a = await ProductSServices.get_usercards();
    if (!this.mounted) return;
    setState(() {
      cards = a;
      show = false;
    });
  }

  @override
  void initState() {
    super.initState();
    get_mycards();
  }

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);

    Widget widget_item(String text, Color color, String icon) => Container(
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(12.5.r)),
          padding: EdgeInsets.only(top: 22.h, bottom: 22.h),
          child: Row(
            children: [
              Container(
                width: 36.w,
              ),
              SvgPicture.asset(icon),
              Container(width: 30.w),
              Text(
                text,
                style: TextStyle(
                    height: 1.2,
                    color: Colors.white,
                    fontFamily: context.locale == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",
                    fontWeight: FontWeight.w400,
                    fontSize: 13.5.sp),
              )
            ],
          ),
        );

    Widget widget_card2(
            {Color color_text,
            String name,
            String code,
            bool check,
            Color color,
            String price}) =>
        GestureDetector(
            onTap: () {
              if (color_text == ColorsApp.col_black) {
                setState(() {
                  color_text = Colors.white;
                  color = ColorsApp.col_app;
                });
              }
            },
            child: Container(
              padding: EdgeInsets.all(16.w),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: ColorsApp.col_gr_back, //color of shadow
                      spreadRadius: 5, //spread radius
                      blurRadius: 7, // blur radius
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                    //you can set more BoxShadow() here
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(16.r)),
                  color: color),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                            height: 1.2,
                            color: color_text,
                            fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                  Container(
                    height: 8.h,
                  ),
                  Row(
                    children: [
                      Text(
                        code,
                        style: TextStyle(
                            height: 1.2,
                            color: color_text,
                            fontSize: 13.8.sp,
                            fontWeight: FontWeight.w800),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Text(
                        "balance".tr().toUpperCase(),
                        style: TextStyle(
                          height: 1.2,
                          color: color_text,
                        ),
                      ),
                      Container(
                        width: 5.w,
                      ),
                      Text(
                        price,
                        style: TextStyle(
                            height: 1.2,
                            color: color_text,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ));

    TextWidget1(String name) {
      return Padding(
          padding: EdgeInsets.only(left: 27.w),
          child: new RichText(
              text: new TextSpan(
                  text: name.split(' ')[0].toUpperCase() + " ",
                  style: new TextStyle(
                      color: ColorsApp.col_black,
                      fontWeight: FontWeight.w800,
                      fontFamily: context.locale == Locale('ar', 'AR')
                          ? "cairo bold"
                          : "rubik medium",
                      fontSize: ScreenUtil().setSp(17)),
                  children: <TextSpan>[
                new TextSpan(
                    text: name.split(' ')[1].toUpperCase(),
                    style: new TextStyle(
                        color: ColorsApp.col_black,
                        fontWeight: FontWeight.w300,
                        fontFamily: context.locale == Locale('ar', 'AR')
                            ? "cairo bold"
                            : "rubik",
                        fontSize: ScreenUtil().setSp(17))),
              ])));
    }

    return Stack(children: [
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(height: 21.h),
        TextWidget1("manage_cards".tr().toUpperCase()),
        Container(height: 21.h),
        Expanded(
            child: show
                ? Center(
                    child: CupertinoTheme(
                    data: CupertinoTheme.of(context)
                        .copyWith(brightness: Brightness.light),
                    child: CupertinoActivityIndicator(),
                  ))
                : ListView(
                    padding: EdgeInsets.all(12.w),
                    children: cards
                        .map((CardModel e) => Column(
                              children: [
                                widget_card2(
                                    color: Colors.white,
                                    check: false,
                                    price: e.label,
                                    name: "Name",
                                    code: e.code,
                                    color_text: Colors.black),
                                Container(
                                  height: 10.h,
                                )
                              ],
                            ))
                        .toList()))
      ]),
      Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
              padding: EdgeInsets.only(bottom: 10.h, left: 12.w, right: 12.w),
              child: SizedBox(
                  height: 64.h,
                  child: PrimaryButton(
                      icon: "assets/icons/plus.png",
                      text: "add_new".tr().toUpperCase(),
                      color_text: ColorsApp.col_app,
                      color: ColorsApp.col_app,
                      onTap: () {
                        if (myProvider.isAuthenticate == false)
                          please_login();
                        else
                          Navigator.push(context, new MaterialPageRoute(
                              builder: (BuildContext context) {
                            return AddCardScreen();
                          }));
                      }))))
    ]);
  }
/**
 * widget_item("add_new".tr(), ColorsApp.col_grey_clear,
    "assets/icons/add.svg"),
 */
}
