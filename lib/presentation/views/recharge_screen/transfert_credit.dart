import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/recharge_screen/transfer_credit_sheet.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class TransfertCredit extends StatefulWidget {
  @override
  _TransfertCreditState createState() => _TransfertCreditState();
}

class _TransfertCreditState extends State<TransfertCredit> {
  @override
  Widget build(BuildContext context) {
    Widget widget_card(
            { Color color_text,
             String name,
             String code,
             Color color,
             String type,
             String price}) =>
        Container(
          padding: EdgeInsets.all(24.w),
          decoration: BoxDecoration(

              boxShadow: [
                BoxShadow(
                  color: ColorsApp.col_gr_back, //color of shadow
                  spreadRadius: 5, //spread radius
                  blurRadius: 7, // blur radius
                  offset: Offset(0, 2), // changes position of shadow
                ),
                //you can set more BoxShadow() here
              ],
              borderRadius: BorderRadius.all(Radius.circular(16.r)),
              color: color),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    name,
                    style: TextStyle(
                        height: 1.2,
                        color: color_text,
                        fontWeight: FontWeight.w800),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    "($type)",
                    style: TextStyle(height: 1.2, color: color_text),
                  ),
                ],
              ),
              Container(
                height: 8.h,
              ),
              Row(
                children: [
                  Text(
                    code,
                    style: TextStyle(
                        height: 1.2,
                        color: color_text,
                        fontWeight: FontWeight.w800),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    "balance".tr().toUpperCase(),
                    style: TextStyle(
                      height: 1.2,
                      color: color_text,
                    ),
                  ),
                  Text(
                    price,
                    style: TextStyle(
                        height: 1.2,
                        color: color_text,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
        );
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Padding(
                padding: EdgeInsets.only(top: 12.h),
                child: SvgPicture.asset(
                  "assets/images/logo.svg",
                  width: 120.w,
                )),
            elevation: 0,
            leading: ArrowButton(),
            actions: [
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset(
                  "assets/icons/notif.svg",
                  color: ColorsApp.col_app,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("assets/icons/wb.svg"),
              ),
            ]),
        body: Container(
            padding: EdgeInsets.all(
              16.w,
            ),
            child: ListView(children: [
              Container(
                  padding: EdgeInsets.only(left: 20.w),
                  child: CustomTexts.TextWidget3(
                      //Cost & purchase Limits
                      'transfer_credit'.tr().split(" ")[0].toUpperCase(),
                      'transfer_credit'.tr().split(" ")[1].toUpperCase(),
                      context.locale)),
              Container(
                height: 10.h,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w),
                child: Text(
                  "login_info".tr(),
                  style: TextStyle(fontSize: 12.sp),
                ),
              ),
              Container(
                height: 32.h,
              ),
              Stack(children: [
                Padding(
                    padding: EdgeInsets.only(left: 23.w, right: 23.w),
                    child: Column(
                      children: [
                        widget_card(
                            color_text: Colors.white,
                            color: ColorsApp.col_app,
                            type: "Benificer",
                            code: "7005430001000581289",
                            name: "Driver",
                            price: "52 KD"),
                        Container(height: 8.h),
                        widget_card(
                            color_text: ColorsApp.col_black,
                            color: Colors.white,
                            type: "Sender",
                            code: "7005430001000581289",
                            name: "SON",
                            price: "52 KD"),
                      ],
                    )),
                Positioned(
                  top: 74.h,
                  child: FloatingActionButton(
                    elevation: 0,
                    backgroundColor: Colors.white,
                    child: SvgPicture.asset("assets/icons/switch.svg"),
                    mini: true,
                    onPressed: () {},
                  ),
                )
              ]),
              Container(
                height: 40.h,
              ),
              Container(
                  height: 90.h,
                  margin: EdgeInsets.only(left: 28.w, right: 28.w),
                  padding: EdgeInsets.only(right: 24.w,left: 24.w),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(16.r)),
                      color: ColorsApp.col_grey_clear),
                  child: Row(
                    children: [
//
                      Text(
                        "amount".tr().toUpperCase(),
                        style: TextStyle(
                            height: 1.2,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Text(
                        "123 KD".toUpperCase(),
                        style: TextStyle(
                            height: 1.2,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                    ],
                  )),
              Container(
                height: 40.h,
              ),
              ChangecreditSheet()
            ])));
  }
}
