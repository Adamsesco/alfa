import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class PayTransferScreen extends StatefulWidget {
  @override
  _PayTransferScreenState createState() => _PayTransferScreenState();
}

class _PayTransferScreenState extends State<PayTransferScreen> {
  List list = [
    {
      "color_text": Colors.white,
      "color": ColorsApp.col_app,
      "name": "DRIVER",
      "CODE": "",
      "price": " 0 KD",
      "check": true,
    },
    {
      "color_text": ColorsApp.col_black,
      "color": ColorsApp.col_white_grey,
      "name": "WIFE",
      "CODE": "",
      "price": " 0 KD",
      "check": false,
    },
    {
      "color_text": ColorsApp.col_black,
      "color": ColorsApp.col_white_grey,
      "name": "DRIVER",
      "CODE": "",
      "price": " 0 KD",
      "check": false,
    }
  ];

  @override
  Widget build(BuildContext context) {


    Widget widget_card2(
            { Color color_text,
             String name,
             String code,
             bool check,
             Color color,
             String price}) =>
        GestureDetector(
            onTap: () {
              if (color_text == ColorsApp.col_black) {
                setState(() {
                  color_text = Colors.white;
                  color = ColorsApp.col_app;
                });
              }
            },
            child: Container(
              padding: EdgeInsets.all(16.w),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: ColorsApp.col_gr_back, //color of shadow
                      spreadRadius: 5, //spread radius
                      blurRadius: 7, // blur radius
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                    //you can set more BoxShadow() here
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(16.r)),
                  color: Colors.white),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                            height: 1.2,
                            color: Colors.black,
                            fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                  Container(
                    height: 8.h,
                  ),
                  Row(
                    children: [
                      Text(
                        code,
                        style: TextStyle(
                            height: 1.2,
                            color: Colors.black,
                            fontSize: 13.8.sp,
                            fontWeight: FontWeight.w800),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Text(
                        "balance".tr().toUpperCase(),
                        style: TextStyle(
                          height: 1.2,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        price,
                        style: TextStyle(
                            height: 1.2,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ));

    Widget widget_item(String text, Color color, String icon) => Container(
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: ColorsApp.col_gr_back, //color of shadow
                  spreadRadius: 5, //spread radius
                  blurRadius: 7, // blur radius
                  offset: Offset(0, 2), // changes position of shadow
                ),
                //you can set more BoxShadow() here
              ],
              color: Colors.white, borderRadius: BorderRadius.circular(12.5.r)),
          padding: EdgeInsets.only(top: 10.h, bottom: 10.h),
          child: Row(
            children: [
              Container(
                width: 36.w,
              ),
              SvgPicture.asset(
                icon,
                color: ColorsApp.col_app,
              ),
              Container(width: 30.w),
              Text(
                text.toUpperCase(),
                style: TextStyle(
                    color: Colors.black,
                    height: 1.2,
                    fontFamily: context.locale == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",
                    fontWeight: FontWeight.w400,
                    fontSize: 12.5.sp),
              ),
              Expanded(
                child: Container(),
              ),
              IconButton(
                  onPressed: () {},
                  icon: SvgPicture.asset("assets/icons/arrow_next.svg"))
            ],
          ),
        );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Padding(
              padding: EdgeInsets.only(top: 12.h),
              child: SvgPicture.asset(
                "assets/images/logo.svg",
                width: 120.w,
              )),
          elevation: 0,
          leading: ArrowButton(),
          actions: [
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(
                "assets/icons/notif.svg",
                color: ColorsApp.col_app,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset("assets/icons/wb.svg"),
            ),
          ]),
      body: ListView(
        padding: EdgeInsets.only(left: 32.w, right: 32.w, top: 16.h),
        children: [
          Container(
              padding: EdgeInsets.only(left: 0.w),
              child: CustomTexts.TextWidget3('Pay '.toUpperCase(),
                  '& transfer'.toUpperCase(), context.locale)),
          Container(
            height: 12.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 0.w),
              child: Container(
                  child: Text(
                "lorem".tr(),
                style: TextStyle(
                    fontSize: 12.sp,
                    height: 1.2,
                    color: const Color(0xff807F82)),
              ))),
          Container(
            height: 12.h,
          ),
          widget_item("transfer_between".tr(), ColorsApp.col_app,
              "assets/icons/switch.svg"),
          Container(
            height: 8.h,
          ),
          widget_item("Alfa Petrol Station Sabahiya", ColorsApp.col_app,
              "assets/icons/cam.svg"),
          Container(
            height: 8.h,
          ),
          widget_item("Alfa Petrol Station AL BIDAA", ColorsApp.col_app,
              "assets/icons/recharge2.svg"),
          Container(
            height: 36.h,
          ),
          Container(
              padding: EdgeInsets.only(left: 0.w),
              child: CustomTexts.TextWidget3(
                  'My '.toUpperCase(), 'cards'.toUpperCase(), context.locale)),
          Container(
            height: 24.h,
          ),
          Column(
              children: list
                  .map((e) => Column(
                        children: [
                          widget_card2(
                              color: e["color"],
                              check: e["check"],
                              price: e["price"],
                              name: e["name"],
                              code: "666556576567575756757575",
                              color_text: e["color_text"]),
                          Container(
                            height: 10.h,
                          )
                        ],
                      ))
                  .toList())
        ],
      ),
    );
  }
}
