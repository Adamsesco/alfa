import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/app/services/products_services.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/custom_dialogs.dart';
import 'package:alpha/presentation/custom_widgets/custom_textfield.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/signin/signin.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:wifi_info_flutter/wifi_info_flutter.dart';

class RechargeScreen extends StatefulWidget {
  @override
  _RechargeScreenState createState() => _RechargeScreenState();
}

class _RechargeScreenState extends State<RechargeScreen>
    with SingleTickerProviderStateMixin {
  List<String> list = ["ALFA TAP", "ALFA PAY", "INDIVIDUAL", "CIVIL ID"];

  final _ccontroller = new TextEditingController();
  final _namecontroller = new TextEditingController();
  FocusNode _focusname = new FocusNode();
  FocusNode _focusconfirm = new FocusNode();
  TabController _tabController;
  bool show = false;

  //final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void showInSnackBar(String value, color) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(
        value,
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
      backgroundColor: color /*Colors.red[900]*/,
    ));
  }

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: list.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);
    please_login() {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text('Please login to conitnue.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(context,
                    new MaterialPageRoute(builder: (BuildContext context) {
                  return SignInScreen();
                }));
              },
              child: Text('Ok'),
            ),
          ],
        ),
      );
    }

    TextWidget(String name1, String name2) {
      return Padding(
          padding: EdgeInsets.only(left: 27.w),
          child: new RichText(
              text: new TextSpan(
                  text: name1,
                  style: new TextStyle(
                      color: ColorsApp.col_black,
                      fontWeight: FontWeight.w800,
                      fontSize: ScreenUtil().setSp(17)),
                  children: <TextSpan>[
                new TextSpan(
                    text: name2,
                    style: new TextStyle(
                        color: ColorsApp.col_black,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(17))),
              ])));
    }

    Widget row(a, b) => Row(
          children: [
            Text(
              a,
              style: TextStyle(fontSize: 14.sp),
            ),
            Expanded(
              child: Container(),
            ),
            Text(
              b,
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 14.sp),
            ),
          ],
        );

    Widget wid = Container(
        height: 200.h,
        child: new Swiper(
          containerHeight: 200.h,
          itemHeight: 206.h,
          containerWidth: MediaQuery.of(context).size.width * 0.9,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              child: ClipRRect(
                  //<--clipping image

                  borderRadius: BorderRadius.all(Radius.circular(16.r)),
                  child: Stack(
                    children: [
                      new Image.asset(
                        "assets/images/card.png",
                        fit: BoxFit.fitHeight,
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 296.h,
                      )
                    ],
                  )),
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  isScrollControlled: true,
                  backgroundColor: Colors.transparent,
                  builder: (context) => Container(
                    height: MediaQuery.of(context).size.height * 0.89,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(25.0),
                        topRight: const Radius.circular(25.0),
                      ),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.all(24.w),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.r)),
                                color: ColorsApp.col_app),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "Alfa Card ID",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w800),
                                    ),
                                    Expanded(
                                      child: Container(),
                                    ),
                                    Text(
                                      "7005430001000581289",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  height: 8.h,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Holder name",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w800),
                                    ),
                                    Expanded(
                                      child: Container(),
                                    ),
                                    Text(
                                      "Abdulsamad Dashti",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 32.h,
                          ),
                          TextWidget("Card  details &", " transactions"),
                          Container(
                            height: 12.h,
                          ),
                          Divider(),
                          row("Date & Time", "05-12-2020   18:43"),
                          Container(
                            height: 8.h,
                          ),
                          row("Transaction type", "Disposite"),
                          Container(
                            height: 8.h,
                          ),
                          row("Amount", "34.800 KD"),
                          Container(
                            height: 8.h,
                          ),
                          row("Service", "Fuel Diesel"),
                          Container(
                            height: 8.h,
                          ),
                          row("Location", "Khaldiya Afla Station"),
                          Container(
                            height: 16.h,
                          ),
                          Divider(),
                          Container(
                            height: 16.h,
                          ),
                          row("Date & Time", "05-12-2020   18:43"),
                          Container(
                            height: 8.h,
                          ),
                          row("Transaction type", "Disposite"),
                          Container(
                            height: 8.h,
                          ),
                          row("Amount", "34.800 KD"),
                          Container(
                            height: 8.h,
                          ),
                          row("Service", "Fuel Diesel"),
                          Container(
                            height: 8.h,
                          ),
                          row("Location", "Khaldiya Afla Station"),
                          Container(
                            height: 8.h,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          },
          itemCount: 3,
          viewportFraction: 0.8,
          scale: 0.9,
        ));

    return ListView(
        padding: EdgeInsets.only(top: 16.w, left: 16.w, right: 16.w),
        children: [
          wid,
          Container(
            height: 12.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(24)),
              child: TabBar(
                  indicator: UnderlineTabIndicator(
                    borderSide:
                        BorderSide(width: 5.0, color: ColorsApp.col_app),
                    insets: EdgeInsets.only(right: 32.0, left: 8.w),
                  ),
                  indicatorColor: ColorsApp.col_app,
                  indicatorPadding: EdgeInsets.all(0),
                  labelPadding: EdgeInsets.all(0),
                  controller: _tabController,
                  unselectedLabelColor: Color(0xffC9D2E0),
                  labelColor: ColorsApp.col_black,
                  indicatorWeight: 12,
                  //indicatorSize: 12,

                  unselectedLabelStyle: TextStyle(color: Color(0xffC9D2E0)),
                  labelStyle: TextStyle(
                      color: ColorsApp.col_black, fontWeight: FontWeight.bold),
                  tabs: list.map((e) => Tab(text: e)).toList())),
          Card(
            elevation: 1,
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(16),
              ),
            ),
            child: Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 12.h,
                ),
                SvgPicture.asset("assets/images/logo_a.svg"),
                Container(height: 30.h),
                Padding(
                    padding: EdgeInsets.only(left: 34.w, right: 34.w),
                    child: Theme(
                        data: ThemeData(
                          hintColor: ColorsApp.col_gr_back,
                        ),
                        child: TextFieldWidget(
                          _namecontroller,
                          _focusname,
                          "code".tr(),
                          false,
                          wid: Container(),
                        ))),
                Padding(
                    padding:
                        EdgeInsets.only(left: 34.w, right: 34.w, top: 10.h),
                    child: Theme(
                        data: ThemeData(
                          hintColor: ColorsApp.col_gr_back,
                        ),
                        child: TextFieldWidget(
                          _ccontroller,
                          _focusname,
                          "abdulsamad.dashti@alfa.com.kw",
                          false,
                          wid: Container(),
                        ))),
                Padding(
                    padding: EdgeInsets.only(
                        left: 34.w, right: 34.w, top: 10.h, bottom: 10.h),
                    child: Theme(
                        data: ThemeData(
                          hintColor: ColorsApp.col_gr_back,
                        ),
                        child: TextFieldWidget(
                          _ccontroller,
                          _focusname,
                          "price".tr(),
                          false,
                          wid: Container(
                              width: 16.w,
                              height: 16.w,
                              padding: EdgeInsets.all(14.w),
                              child: SvgPicture.asset(
                                "assets/icons/arr.svg",
                                width: 12.w,
                              )),
                        ))),
                SizedBox(
                    height: 54.h,
                    width: 189.w,
                    child: PrimaryButton(
                      isLoading: show,
                        text: "racharge".tr().toUpperCase(),
                        color_text: ColorsApp.col_app,
                        color: ColorsApp.col_app,
                        onTap: () async {
                          if (myProvider.isAuthenticate == false)
                            please_login();
                          else {
                            setState(() {
                              show = true;
                            });

                            var wifiIP = await WifiInfo().getWifiIP();

                            print(wifiIP);

                            var a = await ProductSServices.cardRecharge({
                              "client_ip": wifiIP,
                              "payment_method": "knet",
                              "civil_id": "292122000815",
                              "amount": 6
                            });

                            print("-------------------------------");
                            print(a);

                            setState(() {
                              show = false;
                            });

                            if (a["result"] == "false") {
                              showInSnackBar(a["detail"], Colors.red[900]);
                            } else
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                      insetPadding: EdgeInsets.all(8.w),
                                      contentPadding: EdgeInsets.all(8.w),
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(24.0.r))),
                                      title: CustomTexts.TextWidgetTitle(
                                          "order_deetails".tr().toUpperCase(),
                                          context.locale),
                                      content: Container(
                                        height: 450.h,
                                        padding: EdgeInsets.only(
                                            left: 12.w, top: 16.h, right: 12.w),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height:
                                                      ScreenUtil().setWidth(70),
                                                  width:
                                                      ScreenUtil().setWidth(70),
                                                  child: ClipOval(
                                                    child: Image.asset(
                                                      "assets/images/back1.png",
                                                      height: ScreenUtil()
                                                          .setWidth(70),
                                                      width: ScreenUtil()
                                                          .setWidth(70),
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                Divider(),
                                                Container(
                                                  width: 20.w,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "Ahmed Salem",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Container(
                                                      height: 4.h,
                                                    ),
                                                    Text(
                                                      "0633883772",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 12.8.sp),
                                                    ),
                                                    Container(
                                                      height: 4.h,
                                                    ),
                                                    Text(
                                                      "DRIVER",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 12.8.sp),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Container(
                                              height: 20.h,
                                            ),
                                            Divider(),
                                            Container(
                                              height: 8.h,
                                            ),
                                            row("order_number".tr(), "78632"),
                                            Container(
                                              height: 8.h,
                                            ),
                                            row("order_date".tr(),
                                                "05-12-2020   18:43"),
                                            Container(
                                              height: 8.h,
                                            ),
                                            row("price".tr(), "62 KD"),
                                            Container(
                                              height: 8.h,
                                            ),
                                            Divider(),
                                            Container(
                                              height: 40.h,
                                            ),
                                            Center(
                                                child: SizedBox(
                                                    height: 54.h,
                                                    width: 189.w,
                                                    child: PrimaryButton(
                                                        text: "accept_pay"
                                                            .tr()
                                                            .toUpperCase(),
                                                        color_text:
                                                            ColorsApp.col_app,
                                                        color:
                                                            ColorsApp.col_app,
                                                        onTap: () {
                                                          Navigator.pop(
                                                              context);
                                                          CustomDialg
                                                              .show_dialog_connfirm_payment(
                                                                  context);
                                                          /* Navigator.push(context,
                                                      new MaterialPageRoute(builder: (BuildContext context) {
                                                        return RestrictScreen();
                                                      }));*/
                                                        }))),
                                            Container(
                                              height: 24.h,
                                            ),
                                            FlatButton(
                                                onPressed: () {},
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "assets/icons/close.svg",
                                                      color: ColorsApp
                                                          .col_grey_text,
                                                      width: 9.w,
                                                    ),
                                                    Container(
                                                      width: 6.w,
                                                    ),
                                                    Text(
                                                      "decline_offer"
                                                          .tr()
                                                          .toUpperCase(),
                                                      style: TextStyle(
                                                          color: ColorsApp
                                                              .col_grey_text),
                                                    )
                                                  ],
                                                ))
                                          ],
                                        ),
                                      ));
                                },
                              );
                          }
                        })),
                Container(
                  height: 36.h,
                )
              ],
            )),
          )
        ]);
  }
}
