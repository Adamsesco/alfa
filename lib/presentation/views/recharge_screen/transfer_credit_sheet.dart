import 'package:alpha/presentation/custom_widgets/card_tranfer_custom.dart';
import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/recharge_screen/card_list_recharge.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class ChangecreditSheet extends StatefulWidget {
  @override
  _ChangecreditSheetState createState() => _ChangecreditSheetState();
}

class _ChangecreditSheetState extends State<ChangecreditSheet> {

  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
            height: 54.h,
            width: 189.w,
            child: PrimaryButton(
                text: "confirm".tr().toUpperCase(),
                color_text: ColorsApp.col_app,
                color: ColorsApp.col_app,
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    backgroundColor: Colors.transparent,
                    builder: (context) => Container(
                      height: MediaQuery.of(context).size.height * 0.89,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(25.0),
                          topRight: const Radius.circular(25.0),
                        ),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            /* Container(
                              padding: EdgeInsets.all(24.w),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.r)),
                                  color: ColorsApp.col_app),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  Container(
                                    height: 8.h,
                                  ),

                                ],
                              ),
                            ),*/
                            Container(
                              height: 32.h,
                            ),
                            CustomTexts.TextWidget3(
                                "cards_list".tr().split(" ")[0].toUpperCase(),
                                "cards_list".tr().split(" ")[1].toUpperCase(),
                                context.locale),
                            Container(
                              height: 12.h,
                            ),
                            Padding(
                                padding: EdgeInsets.only(left: 0.w),
                                child: Container(
                                    child: Text(
                                  "lorem".tr(),
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      color: const Color(0xff807F82)),
                                ))),
                            Container(
                              height: 12.h,
                            ),
                            CustomTransferCard.widget_card(
                                color_text: ColorsApp.col_black,
                                color: ColorsApp.col_gr_back,
                                code: "7005430001000581289",
                                name: "Driver",
                                price: "52 KD"),
                            Container(height: 8.h),
                            CustomTransferCard.widget_card(
                                color_text: ColorsApp.col_black,
                                color: Colors.white,
                                code: "7005430001000581289",
                                name: "SON",
                                price: "52 KD"),
                            Container(height: 8.h),
                            InkWell(
                                onTap: () {
                                  Navigator.push(context, new MaterialPageRoute(
                                      builder: (BuildContext context) {
                                    return CardListRecharge();
                                  }));
                                },
                                child: CustomTransferCard.widget_card(
                                    color_text: Colors.white,
                                    color: ColorsApp.col_app,
                                    code: "7005430001000581289",
                                    name: "DAUTHER",
                                    price: "52 KD")),
                            Container(height: 8.h),
                            CustomTransferCard.widget_card(
                                color_text: ColorsApp.col_black,
                                color: ColorsApp.col_gr_back,
                                code: "7005430001000581289",
                                name: "Wife",
                                price: "52 KD"),
                            Container(height: 8.h),
                            CustomTransferCard.widget_card(
                                color_text: ColorsApp.col_black,
                                color: ColorsApp.col_gr_back,
                                code: "7005430001000581289",
                                name: "Work",
                                price: "52 KD"),
                            Container(height: 8.h),
                          ],
                        ),
                      ),
                    ),
                  );
                })));
  }
}
