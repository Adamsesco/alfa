import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/drawer_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/recharge_screen/pay_transfer.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class CardListRecharge extends StatefulWidget {
  @override
  _CardListRechargeState createState() => _CardListRechargeState();
}

class _CardListRechargeState extends State<CardListRecharge> {
  List list = [
    {
      "color_text": Colors.white,
      "color": ColorsApp.col_app,
      "name": "DRIVER",
      "CODE": "",
      "price": " 0 KD",
      "check": true,
    },
    {
      "color_text": ColorsApp.col_black,
      "color": ColorsApp.col_white_grey,
      "name": "WIFE",
      "CODE": "",
      "price": " 0 KD",
      "check": false,
    },
    {
      "color_text": ColorsApp.col_black,
      "color": ColorsApp.col_white_grey,
      "name": "DRIVER",
      "CODE": "",
      "price": " 0 KD",
      "check": false,
    }
  ];

  Widget widget_card2(
          {Color color_text,
          String name,
          String code,
          bool check,
          Color color,
          String price}) =>
      GestureDetector(
          onTap: () {
            if (color_text == ColorsApp.col_black) {
              setState(() {
                color_text = Colors.white;
                color = ColorsApp.col_app;
              });
            }
          },
          child: Container(
            padding: EdgeInsets.all(16.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16.r)),
                color: color),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                          height: 1.2,
                          color: color_text,
                          fontWeight: FontWeight.w800),
                    ),
                  ],
                ),
                Container(
                  height: 8.h,
                ),
                Row(
                  children: [
                    Text(
                      code,
                      style: TextStyle(
                          height: 1.2,
                          color: color_text,
                          fontWeight: FontWeight.w800),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      "balance".tr().toUpperCase(),
                      style: TextStyle(
                        height: 1.2,
                        color: color_text,
                      ),
                    ),
                    Text(
                      price,
                      style: TextStyle(
                          height: 1.2,
                          color: color_text,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                check == true
                    ? Container(
                        height: 24.h,
                      )
                    : Container(),
                check == true
                    ? Row(
                        children: [
                          Text(
                            "amount".tr().toUpperCase(),
                            style: TextStyle(
                                height: 1.2,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                          RaisedButton(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0.r))),
                            color: Colors.white.withOpacity(0.2),
                            onPressed: () {},
                            child: Text(
                              "0 KD",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      )
                    : Container(),
              ],
            ),
          ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Padding(
              padding: EdgeInsets.only(top: 12.h),
              child: SvgPicture.asset(
                "assets/images/logo.svg",
                width: 120.w,
              )),
          elevation: 0,
          leading: ArrowButton(),
          actions: [
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(
                "assets/icons/notif.svg",
                color: ColorsApp.col_app,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset("assets/icons/wb.svg"),
            ),
          ]),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 16.h,
          ),
          Container(
              padding: EdgeInsets.only(left: 23.w, right: 23.w),
              child: CustomTexts.TextWidget3('cards_list'.tr().toUpperCase(),
                  'cardslist_recharge'.tr().toUpperCase(), context.locale)),
          Container(
            height: 12.h,
          ),
          Padding(
              padding: EdgeInsets.only(left: 23.w, right: 23.w),
              child: Container(
                  child: Text(
                "lorem".tr(),
                style:
                    TextStyle(fontSize: 12.sp, color: const Color(0xff807F82)),
              ))),
          Expanded(
              child: ListView(
                  padding: EdgeInsets.only(left: 23.w, right: 23.w, top: 24.h),
                  children: list
                      .map((e) => Column(
                            children: [
                              widget_card2(
                                  color: e["color"],
                                  check: e["check"],
                                  price: e["price"],
                                  name: e["name"],
                                  code: "666556576567575756757575",
                                  color_text: e["color_text"]),
                              Container(
                                height: 10.h,
                              )
                            ],
                          ))
                      .toList())),
          Container(
              height: 70.h,
              margin: EdgeInsets.only(left: 28.w, right: 28.w),
              padding: EdgeInsets.only(left: 24.w, right: 24.w),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(16.r)),
                  color: ColorsApp.col_white_grey),
              child: Row(
                children: [
//
                  Text(
                    "total_amount".tr().toUpperCase(),
                    style: TextStyle(
                        height: 1.2,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                 Text(
                      "0 KD".toUpperCase(),
                      style: TextStyle(
                          height: 1.2,
                          fontWeight: FontWeight.w900,
                          color: Colors.black),
                    ),

                ],
              )),
          Container(
            height: 40.h,
          ),
          Center(
              child: SizedBox(
                  height: 54.h,
                  width: 189.w,
                  child: PrimaryButton(
                      text: "confirm".tr().toUpperCase(),
                      color_text: ColorsApp.col_app,
                      color: ColorsApp.col_app,
                      onTap: () {
                        Navigator.push(context, new MaterialPageRoute(
                            builder: (BuildContext context) {
                          return PayTransferScreen();
                        }));
                      }))),
          Container(
            height: 40.h,
          )
        ],
      ),
    );
  }
}
