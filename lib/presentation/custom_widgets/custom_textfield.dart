import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextFieldWidget extends StatelessWidget {
  TextFieldWidget(
      this.ctrl, this.focus, this.text, this.bl,{this.onsaved, this.validate,  this.wid});

  var ctrl;
  var focus;
  var validate;
  Widget wid;

  var text;
  bool bl;
  var  onsaved;


  @override
  Widget build(BuildContext context) {
    return Container(

        constraints: BoxConstraints(
        minWidth: double.infinity,
    ),
    padding: EdgeInsets.all(16.h),
    decoration: BoxDecoration(
    boxShadow: [
    BoxShadow(
    color: ColorsApp.col_gr_back, //color of shadow
    spreadRadius: 5, //spread radius
    blurRadius: 7, // blur radius
    offset: Offset(0, 2), // changes position of shadow
    ),
    //you can set more BoxShadow() here
    ],
    borderRadius: BorderRadius.circular(10.5.r),
    color: Colors
        .white /*widget.enabled
              ? (widget.color ??
                  (widget.inverted ? Colors.transparent : ColorsApp.col_app))
              : (widget.disabledColor ? ColorsApp.col_grey_clear)*/
    ,

    ),
    child: TextFormField(
      style: new TextStyle(fontSize: 15.0, color: Colors.black),
      controller: ctrl,
      focusNode: focus,
      obscureText: bl,
      keyboardType: TextInputType.text,
      validator: validate,
      onChanged: (val){
        if(onsaved != null)
        onsaved(val);
      },
      onSaved: (val) {
        ctrl.text = val;
        if(onsaved != null)
          onsaved(val);
      },
      onFieldSubmitted: (val) {
        ctrl.text = val;
      },
      decoration:
      new InputDecoration.collapsed(
         /* suffixIcon: GestureDetector(
              onTap: () {
                //show();
              },
              child:wid==null?Container(): wid),*/

         /* border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),*/
         // filled: true,
          hintStyle: new TextStyle(color: ColorsApp.col_gr),
          hintText: text),



    ));
  }
}
