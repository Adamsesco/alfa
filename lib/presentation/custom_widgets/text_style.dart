import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:easy_localization/easy_localization.dart';

class CustomTexts {
  static Widget row(a, b) =>
      Row(
        children: [
          Text(
            a,
            style: TextStyle(fontSize: 14.sp),
          ),
          Expanded(
            child: Container(),
          ),
          Text(
            b,
            style: TextStyle(fontWeight: FontWeight.w800, fontSize: 14.sp),
          ),
        ],
      );


  static Widget row2(a, b) =>
      Row(
        children: [
          Text(
            a,
            style: TextStyle(fontSize: 14.sp, fontWeight: FontWeight.w900),
          ),
          Expanded(
            child: Container(),
          ),
          Text(
            b,
            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 14.sp),
          ),
        ],
      );

  static TextWidgetTitle(String name, ctx) {
    return Padding(
        padding: EdgeInsets.only(left: 27.w),
        child: new RichText(
            text: new TextSpan(
                text: name.toUpperCase() + " ",
                style: new TextStyle(
                    fontFamily: ctx == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",

                    color: ColorsApp.col_black,
                    fontWeight: FontWeight.w800,
                    fontSize: ScreenUtil().setSp(16)),
                children: <TextSpan>[])));
  }


  static TextWidget0(String name, ctx) {
    return Padding(
        padding: EdgeInsets.only(left: 27.w, right: 27.w),
        child: new RichText(

            text: new TextSpan(
                text: name.split(' ')[0].toUpperCase() + " ",

                style: new TextStyle(
                    color: ColorsApp.col_black,
                    fontWeight: FontWeight.w800,
                    fontFamily: ctx == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",
                    fontSize: ScreenUtil().setSp(17)),
                children: <TextSpan>[
                  new TextSpan(
                      text: ctx == Locale('ar', 'AR')
                          ? ""
                          : name.split(' ')[1].toUpperCase(),
                      style: new TextStyle(
                          color: ColorsApp.col_black,
                          fontFamily: ctx == Locale('ar', 'AR')
                              ? "cairo bold"
                              : "rubik medium",
                          fontWeight: FontWeight.w400,
                          fontSize: ScreenUtil().setSp(17))),
                ])));
  }


  static TextWidget(String name, ctx) {
    return Padding(
        padding: EdgeInsets.only(left: 27.w, right: 27.w),
        child: new RichText(
            text: new TextSpan(
                text: name.split(' ')[0].toUpperCase() + " ",
                style: new TextStyle(
                    color: ColorsApp.col_black,
                    fontFamily: ctx == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",

                    fontWeight: FontWeight.w800,
                    fontSize: ScreenUtil().setSp(17)),
                children: <TextSpan>[
                  new TextSpan(
                      text: name.split(' ')[1].toUpperCase(),
                      style: new TextStyle(
                          color: ColorsApp.col_black,
                          fontWeight: FontWeight.w400,
                          fontSize: ScreenUtil().setSp(17))),
                ])));
  }

  static TextWidget3(String name1, String name2, ctx) {
    return Padding(
        padding: EdgeInsets.only(left: 0.w),
        child: new RichText(
            text: new TextSpan(
                text: name1.toUpperCase() + " ",
                style: new TextStyle(
                    color: ColorsApp.col_black,
                    fontFamily: ctx == Locale('ar', 'AR')
                        ? "cairo bold"
                        : "rubik medium",

                    fontWeight: FontWeight.w800,
                    fontSize: ScreenUtil().setSp(16.5)),
                children: <TextSpan>[
            new TextSpan(
            text: name2.toUpperCase(),
                style: new TextStyle(
                    color: ColorsApp.col_black,
                    fontWeight: FontWeight.w300,
                    fontFamily: ctx == Locale('ar', 'AR') ? "cairo bold" : "rubik",
                fontSize: ScreenUtil().setSp(16.5))),
        ])));
  }

  static TextWidget4(String name1, String name2) {
    return Padding(
        padding: EdgeInsets.only(left: 0.w),
        child: new RichText(
            text: new TextSpan(
                text: name1.toUpperCase() + " ",
                style: new TextStyle(
                    color: ColorsApp.col_black,
                    fontWeight: FontWeight.w800,
                    fontSize: ScreenUtil().setSp(17.5)),
                children: <TextSpan>[
                  new TextSpan(
                      text: name2.toUpperCase(),
                      style: new TextStyle(
                          color: ColorsApp.col_black,
                          fontWeight: FontWeight.w400,
                          fontSize: ScreenUtil().setSp(17.5))),
                ])));
  }
}
