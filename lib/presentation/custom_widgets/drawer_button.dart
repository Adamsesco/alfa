import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ArrowButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: SvgPicture.asset(context.locale == Locale('ar', 'AR')
          ? "assets/icons/arrow_a.svg"
          : "assets/icons/arrow_back.svg"),
    );
  }
}


class DrawerButton extends StatelessWidget {
  DrawerButton({this.onPressed});
  var onPressed;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        onPressed();
        },
      icon: SvgPicture.asset(context.locale == Locale('ar', 'AR')
          ? "assets/icons/drawer_a.svg"
          : "assets/icons/menu.svg"),
    );
  }
}
