import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomTransferCard{

  static   Widget widget_card(
      { Color color_text,
         String name,
         String code,
         Color color,
         String price}) =>
      Container(
        padding: EdgeInsets.all(16.w),
        decoration: BoxDecoration(

            borderRadius: BorderRadius.all(Radius.circular(16.r)),
            color: color),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  name,
                  style: TextStyle(
                      color: color_text, fontWeight: FontWeight.w800),
                ),


              ],
            ),
            Container(
              height: 8.h,
            ),
            Row(
              children: [
                Text(
                  code,
                  style: TextStyle(
                      color: color_text, fontWeight: FontWeight.w800),
                ),
                Expanded(
                  child: Container(),
                ),
                Text(
                  "BALANCE ",
                  style: TextStyle(
                    color: color_text,
                  ),
                ),
                Text(
                  price,
                  style: TextStyle(
                      color: color_text, fontWeight: FontWeight.bold),
                ),
              ],
            )
          ],
        ),
      );



  static   Widget widget_card2(
      { Color color_text,
         String name,
         String code,
         Color color,
         String price}) =>
      Container(
        padding: EdgeInsets.all(16.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(16.r)),
            color: color),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  name,
                  style: TextStyle(
                      color: color_text, fontWeight: FontWeight.w800),
                ),


              ],
            ),
            Container(
              height: 8.h,
            ),
            Row(
              children: [
                Text(
                  code,
                  style: TextStyle(
                      color: color_text, fontWeight: FontWeight.w800),
                ),
                Expanded(
                  child: Container(),
                ),
                Text(
                  "BALANCE ",
                  style: TextStyle(
                    color: color_text,
                  ),
                ),
                Text(
                  price,
                  style: TextStyle(
                      color: color_text, fontWeight: FontWeight.bold),
                ),
              ],
            )
          ],
        ),
      );
}