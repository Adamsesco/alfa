import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DottedSlider extends StatefulWidget {
  final Color color;
  final List<Widget> children;
  final double maxHeight;

  DottedSlider(
      {this.color = Colors.white, this.children, this.maxHeight = 100.0})
      : super();

  @override
  _DottedSliderState createState() => new _DottedSliderState();
}

class _DottedSliderState extends State<DottedSlider> {
  PageController controller = PageController();

  double currentPage = 0;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      setState(() {
        currentPage = controller.page;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: widget.maxHeight,
          //  minWidth: MediaQuery.of(context).size.width,
          //  maxWidth: MediaQuery.of(context).size.width),
          child: PageView(
            //  pageSnapping: false,

            controller: controller,
            children: widget.children,
          ),
        ),
        Container(height: 12),
        _drawDots(currentPage),
      ],
    );
  }

  _drawDots(page) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        for (int i = 0; i < widget.children.length; i++) dot((page == i)),
      ],
    );
  }

  dot(bool selected) {
    double size = 12;
    return Container(
      padding: const EdgeInsets.all(2.5),
      child: Container(
        width: selected ? 10 : 8,
        height: selected ? 10 : 8,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: (selected) ? Colors.grey : Colors.black54),
      ),
    );
  }
}
