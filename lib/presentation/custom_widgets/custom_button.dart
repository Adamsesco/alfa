import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PrimaryButton extends StatefulWidget {
  final bool inverted;
  final String text;
  final Color color;
  final Color color_text;
  final String icon;

  final Color disabledColor;
  final TextStyle textStyle;
  final bool enabled;
  final bool isLoading;
  void Function() onTap;

  PrimaryButton(
      {this.inverted = false,
      this.text = "",
      this.icon,
      this.color = ColorsApp.col_app,
      this.color_text = Colors.white,
      this.disabledColor = Colors.white,
      this.textStyle = const TextStyle(),
      this.enabled = true,
      this.isLoading = false,
      this.onTap});

  @override
  _SecPrimaryButtonState createState() => _SecPrimaryButtonState();
}

class _SecPrimaryButtonState extends State<PrimaryButton> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: InkWell(
      onTap: widget.enabled ? widget.onTap : null,
      child: Container(
        constraints: BoxConstraints(
          minWidth: double.infinity,
        ),
        padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setHeight(15).toDouble(),
        ),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: ColorsApp.col_gr_back, //color of shadow
              spreadRadius: 5, //spread radius
              blurRadius: 7, // blur radius
              offset: Offset(0, 2), // changes position of shadow
            ),
            //you can set more BoxShadow() here
          ],
          borderRadius: BorderRadius.circular(10.5.r),
          color: Colors
              .white /*widget.enabled
              ? (widget.color ??
                  (widget.inverted ? Colors.transparent : ColorsApp.col_app))
              : (widget.disabledColor ? ColorsApp.col_grey_clear)*/
          ,
          border: !widget.inverted || !widget.enabled
              ? null
              : Border.all(
                  color: Colors.white,
                  width: 1,
                ),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            /* if (widget.prefix != null)
              Positioned.directional(
                textDirection: Directionality.of(context),
                start: ScreenUtil().setWidth(37).toDouble(),
                child: widget.prefix,
              ),*/
            if (widget.isLoading)
              CupertinoTheme(
                data: CupertinoTheme.of(context)
                    .copyWith(brightness: Brightness.light),
                child: CupertinoActivityIndicator(),
              ),
            Visibility(
              visible: !widget.isLoading,
              maintainAnimation: true,
              maintainSize: true,
              maintainState: true,
              child: Center(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  widget.icon != null ? Image.asset(widget.icon) : Container(),
                  Container(width: widget.icon != null ? 24.w : 0),
                  Text(
                    widget.text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      height: context.locale == Locale('ar', 'AR') ? 1.4 : 1.2,
                      fontSize: ScreenUtil().setSp(16).toDouble(),
                      fontFamily: context.locale == Locale('ar', 'AR')
                          ? "cairo bold"
                          : "rubik medium",
                      fontWeight: FontWeight.w400,
                      color: widget.color_text,
                    ),
                  ),
                ],
              )),
            ),
          ],
        ),
      ),
    ));
  }
}
