import 'package:alpha/presentation/custom_widgets/custom_button.dart';
import 'package:alpha/presentation/custom_widgets/text_style.dart';
import 'package:alpha/presentation/views/recharge_screen/transfert_credit.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:easy_localization/easy_localization.dart';

class CustomDialg {
  static show_dialog_connfirm_payment(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            insetPadding: EdgeInsets.all(8.w),
            contentPadding: EdgeInsets.all(8.w),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24.0.r))),
            title: CustomTexts.TextWidgetTitle("order_deetails".tr().toUpperCase(),context.locale),
            content: Container(
              height: 450.h,
              padding: EdgeInsets.only(left: 24.w, top: 16.h, right: 24.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 20.h,
                  ),
                  Divider(),
                  Container(
                    height: 8.h,
                  ),
                  CustomTexts.row("order_number".tr(), "78632"),
                  Container(
                    height: 8.h,
                  ),
                  CustomTexts.row("customer_name".tr(), "Abdulsamad Dashti"),
                  Container(
                    height: 8.h,
                  ),
                  CustomTexts.row("trans_name".tr(), "05-12-2020   18:43"),
                  Container(
                    height: 8.h,
                  ),
                  CustomTexts.row("pay_meth".tr(), "Cash"),
                  Container(
                    height: 8.h,
                  ),
                  CustomTexts.row("price".tr(), "62 KD"),
                  Container(
                    height: 8.h,
                  ),
                  Divider(),
                  Container(
                    height: 40.h,
                  ),
                  Center(
                      child: SizedBox(
                          height: 54.h,
                          width: 189.w,
                          child: PrimaryButton(
                              text: "confirm".tr().toUpperCase(),
                              color_text: ColorsApp.col_app,
                              color: ColorsApp.col_app,
                              onTap: () {
                                Navigator.push(context,
                                    new MaterialPageRoute(builder: (BuildContext context) {
                                      return TransfertCredit();
                                    }));
                              }))),
                  Container(
                    height: 24.h,
                  ),
                  FlatButton(
                      onPressed: () {

                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            "assets/icons/close.svg",
                            color: ColorsApp.col_grey_text,
                            width: 9.w,
                          ),
                          Container(
                            width: 6.w,
                          ),
                          Text(
                            "cancel_order".tr().toUpperCase(),
                            style: TextStyle(color: ColorsApp.col_grey_text),
                          )
                        ],
                      ))
                ],
              ),
            ));
      },
    );
  }
}
