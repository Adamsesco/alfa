import 'package:alpha/app/services/rest_services.dart';
import 'package:alpha/models/entity/notif_app.dart';
import 'package:alpha/models/entity/user.dart';

import 'package:shared_preferences/shared_preferences.dart';

class Settings_Services {
  static RestService rest = new RestService();

  user_informations(id) async {
    var pr_resp = await rest.get('users/' + id);
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    Map<String, dynamic> sp = pr_resp;
    return new User.fromMap(sp);
  }

  static get_all_notifications() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('notifications', {"email": "$email"});
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    if (sp == null)
      return List<NotifApp>.from([]);
    else
      return sp
          .map((var contactRaw) => new NotifApp.fromMap(contactRaw))
          .toList();
  }

  user_info() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String id = prefs.getString("id");
    var pr_resp = await rest.get('users/' + id);
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    print(pr_resp);
    Map<String, dynamic> sp = pr_resp;
    return new User.fromMap(sp);
  }
}
