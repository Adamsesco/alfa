import 'dart:convert';

import 'package:alpha/app/services/rest_services.dart';
import 'package:alpha/models/entity/card_model.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:shared_preferences/shared_preferences.dart';

class ProductSServices {
  static RestService rest = new RestService();



  static civilIdTransactions(json) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('civilIdTransactions', json);

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;

    print(sp);
  }

  static civilIdRecharge(json) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('civilIdRecharge', json);

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;

    print(sp);
  }


  static cardRecharge(json) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('civilIdRecharge', json);

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    return pr_resp;

  }

  static createCivilIDCustomer(json) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('createCivilIDCustomer', json);

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";



    return pr_resp;
  }


  static get_usercards() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('userCards', {"email": "$email"});

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    if (sp == null)
      return List<CardModel>.from([]);
    else
      return sp
          .map((var contactRaw) => new CardModel.fromMap(contactRaw))
          .toList();
  }

//http://morocode.com/limited/api/purchases/4
 /* get_purchases() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var id = prefs.getString("id");
    var pr_resp = await rest.get('purchases/4');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";


    var sp = pr_resp;
    print(sp);

    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }



  getMarket() async {
    ///http://morocode.com/limited/api/shop/2/analytics/01-12-2020/01-01-2021

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var id = prefs.getString("id");
    DateTime dte = DateTime.now();
    String startDate = DateFormat('dd-mm-yyyy')
        .format(dte);
    String endDate = DateFormat('dd-mm-yyyy')
        .format(dte.add(Duration(days: 30)));
    var pr_resp = await rest.get('shop/$id/analytics/$startDate/$endDate');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    var sp = pr_resp;

    if (sp == null)
      return Dashboard1();
    else {

      print("------");
      return new Dashboard1.fromMap(pr_resp);
    }


  }

  get_bids() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var id = prefs.getString("id");
    var pr_resp = await rest.get('bids/$id');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";


    var sp = pr_resp;

    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }


  get_store_deetails(String id) async {
    var pr_resp = await rest.get('shop/$id');

    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";


    var sp = pr_resp;
    if (sp == null)
      return List<Store>.from([]);
    else
      return new Store.fromMap(sp);
  }

  switch_to_shop() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var id = prefs.getString("login_token");

    var a = await rest.post("switchToShop", json.encode({"token": id}));

    // http://morocode.com/limited/api/switchToShop
  }


  slider_shop(sliders,avatar) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var id = prefs.getString("login_token");

    var a = await rest.post("updateShop", json.encode({"token": id, "slider": sliders,"avatar":avatar}));

    print("----");
    print(a);
    // http://morocode.com/limited/api/switchToShop
  }


  get_product_by_category(String id) async {
    var pr_resp = await rest.get('category/$id');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["products"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }


  get_all_stores() async {
    var pr_resp = await rest.get('shops/');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["stores"];
    if (sp == null)
      return List<Store>.from([]);
    else
      return sp
          .map((var contactRaw) => new Store.fromMap(contactRaw))
          .toList();
  }

  get_all_product() async {
    var pr_resp = await rest.get('Allproducts');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["products"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }




  get_product_by_favorites(String token) async {
    var pr_resp = await rest.get('saved/?token=$token');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["data"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }*/
}
