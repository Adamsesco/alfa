import 'dart:convert';

import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/app/services/storage_util.dart';
import 'package:alpha/models/entity/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigApp {
  static String base_url = "http://216.250.119.123:3001/api/";
}

class RestService {
  final JsonDecoder _decoder = new JsonDecoder();
  StorageUtil storageUtil = StorageUtil();

  put(url2, data) async {
    var con = true;

    var results;
    if (con) {
      //data = JSON.encode(data);
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.put(url1, body: data);
      String jsonBody = response.body;

      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = jsonDecode(jsonBody);
        results = postsContainer;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }

  get(url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(
        ConfigApp.base_url + url2,
      ));

      var response =
          await http.get(url1, headers: {"content-type": "application/json"});

      print(url1);

      String jsonBody = response.body;
      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        results = _decoder.convert(jsonBody);
      }
    } else {
      results = "No Internet";
    }
    return results;
  }

//headers: {"content-type": "application/json"},
  post(url2, data) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(
        Uri.encodeFull(ConfigApp.base_url + url2),
      );

      print(url1);
      var response = await http.post(
        url1,
        body: json.encode(data),
        headers: {"content-type": "application/json"},
      );
      String jsonBody = response.body;

      print(response.body);
      var statusCode = response.statusCode;

      var postsContainer = _decoder.convert(jsonBody);
      results = postsContainer;
    } else {
      results = "No Internet";
    }
    return results;
  }

  post2(url2, data) async {
    var con = true;
    var results;
    if (con) {
      var url1 =
          Uri.parse(Uri.encodeFull("http://morocode.com/limited/" + url2));

      var response = await http.post(url1, body: data);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      /* if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {*/
      var postsContainer = _decoder.convert(jsonBody);
      results = postsContainer;
    } else {
      results = "No Internet";
    }
    return results;
  }

  post_register(url2, data) async {
    var con = true;
    int statusCode;

    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.post(
        url1,
        body: data,
        headers: {"content-type": "application/json"},
      );
      String jsonBody = response.body;
      print(jsonBody);
/*
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("id", json.decode(jsonBody)["results"]["id"]);*/

      statusCode = json.decode(jsonBody)["status"];
    } else {
      statusCode = 190;
    }
    return statusCode;
  }

  get_fav(item, String user) async {
    print({"product_id": "$item", "token": "$user"});

    var js = json.encode({"product_id": "$item", "token": "$user"});
    var a = await post("addtoSaveCart", js);
    return a;
  }

  /*check_fav(item, user) async {
    var js = {"item_id": "$item", "user_id": "$user"};
    var a = await get("users/fav/check?item_id=$item&user_id=$user");
    return a;

  }*/

  post_login_new(url2, func, context, data) async {
    var con = true;
    int statusCode;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.post(
        url1,
        body: data,
        headers: {"content-type": "application/json"},
      );
      String jsonBody = response.body;

      statusCode = json.decode(jsonBody)["status"];

      /* if(json.decode(jsonBody)["status"] == 200)
      {

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("id", json.decode(jsonBody)["data"]["id"]);
        prefs.setString("login_token", json.decode(jsonBody)["data"]["login_token"]);

        print(json.decode(jsonBody)["data"]["id"]);

        if(func.toString() != "null")
        {
          func(json.decode(jsonBody)["data"]["id"]);
        }
        Navigator.pop(context);


      }*/
      if (statusCode == 400 && json.decode(jsonBody)["code"] == 2) {
        statusCode = 201;
      } else if (statusCode == 400 && json.decode(jsonBody)["code"] == 3) {
        statusCode = 209;
      }
    } else {
      statusCode = 190;
    }
    return statusCode;
  }



  post_signup(url2, context, data) async {
    var con = true;
    int statusCode;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));

      print(url1);
      var response = await http.post(
        url1,
        body: json.encode(data),
        headers: {"content-type": "application/json"},
      );
      String jsonBody = response.body;
      print(response.body);

      statusCode = json.decode(jsonBody)["status"];


      if (json.decode(jsonBody)["Result"] == "true") {

        return true;
      }
      else return false;
    }
  }

    post_login(url2, context, data) async {
    var con = true;
    int statusCode;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));

      print(url1);
      var response = await http.post(
        url1,
        body: json.encode(data),
        headers: {"content-type": "application/json"},
      );
      String jsonBody = response.body;
      print(response.body);

      statusCode = json.decode(jsonBody)["status"];

      print(response.body);

      if (json.decode(jsonBody)["Result"] == "true") {
        storageUtil.inicializar().then((_) async {
          var myProvider = Provider.of<LoginProvider>(context, listen: false);
          myProvider.mitexto = 'Provider';
          myProvider.isAuthenticate = true;
          storageUtil.putBool('isAutenticado', true);

          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("id", data["email"]);
          prefs.setString("avatar", json.decode(jsonBody)["Avatar"]);
          prefs.setString("username", json.decode(jsonBody)["FirstName"]);

          prefs.setString("mobile", json.decode(jsonBody)["Mobile"]);
          prefs.setString("email", data["email"]);

          myProvider.user = User(
              prefs.getString("id"),
              prefs.getString("username"),
              prefs.getString("avatar"),
              "",
              DateTime.now(),
              prefs.getString("mobile"),
              prefs.getString("username"));
          Navigator.pop(context);
        });
      } else {
        return 400;
      }
      return statusCode;
    }
  }

  delete(url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.delete(url1);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = _decoder.convert(jsonBody);
        results = postsContainer.length == 0;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }
}
