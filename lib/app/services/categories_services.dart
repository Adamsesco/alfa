import 'package:alpha/app/services/rest_services.dart';
import 'package:alpha/models/entity/category.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CategoriesService {
  RestService rest = new RestService();

  get_all_governorates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("id");

    var pr_resp = await rest.post('governorates', {"email": "$email"});
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    return sp
        .map((var contactRaw) => new Category.fromMap(contactRaw))
        .toList();
  }

  get_areas(cat_id) async {
    var pr_resp = await rest.post('areas' ,{"governorate_id":2});
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    if (sp == null)
      return [];
    else
      return sp
          .map((var contactRaw) => new Category.fromMap(contactRaw))
          .toList();
  }


/* get_all_categories() async {
    var pr_resp = await rest.get('categories');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    return sp
        .map((var contactRaw) => new Category.fromMap(contactRaw))
        .toList();
  }

 */

}
