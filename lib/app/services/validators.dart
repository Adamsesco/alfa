import 'package:connectivity/connectivity.dart';


/*

SpinKitFadingCircle(
  itemBuilder: (_, int index) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: index.isEven ? Colors.red : Colors.green,
      ),
    );
  },
);
 */

class AppService {
  AppService({this.context});
  var context;

  isconnected() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  String validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return 'أدخل البريد الإلكتروني من فضلك';
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return "";
    }
    return 'البريد الإلكتروني غير صالح';
  }


  String validatePassword2(String value) {
    if (value.length > 3) {
      return "";
    }
    return 'يجب أن تكون كلمة المرور من 4 أحرفّ';
  }


  String validatePassword(String value) {
    if (value.length > 5) {
      return "";
    }
    return 'يجب أن تكون كلمة المرور 6 أحرف على الأقلّ';
  }

  String validatename(String value) {
    if (value.isEmpty) return 'أدخل الاسم من فضلك';

    return "";
  }

  String vali(String value) {

    return "";
  }


  String filll(String value) {
    if (value.isEmpty) return "من فضلك املأ الفراغ";

    return "";
  }



  String validatephonenumber(String value) {
    if (value.length > 0) {
      return "";
    }
    return 'أدخل رقم الهاتف من فضلك';
  }


  String validateAddress(String value) {
    if (value.length > 0) {
      return "";
    }
    return 'أدخل الاسم من فضلك';
  }




  String validateDesc(String value) {
    if (value.length > 0) {
      return "";
    }
    return "ادخل الوصف من فضلك";
  }





}