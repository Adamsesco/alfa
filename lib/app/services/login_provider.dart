import 'package:alpha/models/entity/user.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginProvider with ChangeNotifier {
  SharedPreferences _prefs;

  LoginProvider() {
    _loadFromPrefs();
  }

  _initPrefs() async {
    if (_prefs == null) _prefs = await SharedPreferences.getInstance();
  }

  _loadFromPrefs() async {
    await _initPrefs();
    isAuthenticate = _prefs.getBool('isAutenticado') == true ? true : false;

    notifyListeners();
  }

  bool _isAutenticate = false;
  String _mitexto = "Provider";
  User _user;

  String get mitexto => _mitexto;

  bool get isAuthenticate => _isAutenticate;

  User get user => _user;

  set user(User user) {
    _user = user;
    notifyListeners();
  }

  set mitexto(String newTexto) {
    _mitexto = newTexto;

    notifyListeners();
  }

  set isAuthenticate(bool newAutenticado) {
    _isAutenticate = newAutenticado;
    if (_isAutenticate == true) {
      _user = new User(
          _prefs.getString("id"),
          _prefs.getString("username") ,
          _prefs.getString("avatar")==null? "https://res.cloudinary.com/dgxctjlpx/image/upload/v1618096841/placeholder_itp3gm.png":_prefs.getString("avatar"),
          "",
          DateTime.now(),
          "",
          _prefs.getString("username"));
    }
    // _id = _prefs.getString("id");
    notifyListeners();
  }
}
