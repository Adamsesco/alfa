import 'package:shared_preferences/shared_preferences.dart';

class StorageUtil {
   SharedPreferences _preferences;

   Future<void> inicializar() async {
    _preferences = await SharedPreferences.getInstance();
  }

   String getString(String key, {String defValue = ''}) {
    if (_preferences == null) return defValue;
    return _preferences.getString(key) ?? defValue;
  }

    putString(String key, String value) {
    if (_preferences == null) return null;
    return _preferences.setString(key, value);
  }

   bool getBool(String key, {bool defValue = false}) {
    if (_preferences == null) return defValue;
    return _preferences.getBool(key) ?? defValue;
  }

    putBool(String key, bool value) {
    if (_preferences != null)
     _preferences.setBool(key, value);
  }


}
