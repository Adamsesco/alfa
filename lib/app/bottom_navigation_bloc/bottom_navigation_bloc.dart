import 'dart:async';
import 'package:alpha/app/bottom_navigation_bloc/bottom_navigation_event.dart';
import 'package:alpha/app/bottom_navigation_bloc/bottom_navigation_state.dart';
import 'package:bloc/bloc.dart';

class BottomNavigationBarBloc
    extends Bloc<BottomNavigationBarEvent, BottomNavigationBarState> {
  BottomNavigationBarBloc() : super();

  @override
  Stream<BottomNavigationBarState> mapEventToState(
    BottomNavigationBarEvent event,
  ) async* {
    if (event is LoadHome) {
      yield HomeState();
    }
    if (event is LoadLocations) {
      yield LocationsState();
    }
    if (event is LoadServices) {
      yield ServicesState();
    }
    if (event is LoadDashboard) {
      yield DashboardState();
    }
  }

  @override
  // TODO: implement initialState
  BottomNavigationBarState get initialState => throw UnimplementedError();
}
