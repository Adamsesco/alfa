import 'package:equatable/equatable.dart';

abstract class BottomNavigationBarEvent extends Equatable {
  const BottomNavigationBarEvent();
}

class LoadHome extends BottomNavigationBarEvent{
  @override
  List<Object> get props => [];

}
class LoadLocations extends BottomNavigationBarEvent{
  @override
  List<Object> get props => [];

}
class LoadServices extends BottomNavigationBarEvent{
  @override
  List<Object> get props => [];

}
class LoadDashboard extends BottomNavigationBarEvent{
  @override
  List<Object> get props => [];

}
