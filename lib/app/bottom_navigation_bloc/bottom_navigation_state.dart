import 'package:equatable/equatable.dart';
import 'package:easy_localization/easy_localization.dart';

abstract class BottomNavigationBarState extends Equatable {
  const BottomNavigationBarState();
}

class HomeState extends BottomNavigationBarState {
  final int index = 0;
  final String title = 'Home';

  @override
  List<Object> get props => [index, title];
}

class LocationsState extends BottomNavigationBarState {
  final int index = 1;
  final String title = 'Locations';

  @override
  List<Object> get props => [index, title];
}

class RechargeState extends BottomNavigationBarState {
  final int index = 2;
  final String title = 'Recharge';

  @override
  List<Object> get props => [index, title];
}

class ServicesState extends BottomNavigationBarState {
  final int index = 3;
  final String title = 'Services';

  @override
  List<Object> get props => [index, title];
}



class DashboardState extends BottomNavigationBarState {
  final int index = 4;
  final String title = 'Dashboard';

  @override
  List<Object> get props => [index, title];
}
