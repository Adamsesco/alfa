import 'package:alpha/app/services/login_provider.dart';
import 'package:alpha/presentation/views/slides/introduction_screen.dart';
import 'package:alpha/utils/styles/colors_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart' as p;
import 'package:provider/provider.dart';


void main()  {
  WidgetsFlutterBinding.ensureInitialized();

  //EasyLocalization.ens();

  runApp(EasyLocalization(
      supportedLocales: [Locale('ar', 'AR'),Locale('en', 'US')],
      path: 'assets/translations',
      fallbackLocale: Locale('en', 'US'),
      startLocale: Locale('ar', 'AR'),
      useOnlyLangCode: true,
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: Size(375, 812),
        builder: () =>  p.MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (_) => LoginProvider(),
              )
            ],
            child: MaterialApp(
                title: 'ALFA',
                debugShowCheckedModeBanner: false,
                locale: context.locale,
                supportedLocales: [Locale('ar', 'AR'),Locale('en', 'US')],
                localizationsDelegates: context.localizationDelegates,
                theme: ThemeData(
                  fontFamily: context.locale==Locale('ar', 'AR')?"cairo":"rubik",
                  primaryColor: ColorsApp.col_app,
                ),
                home:
                /*MultiRepositoryProvider(
                            providers: [
                              /* RepositoryProvider<LoginRepository>(
        create: (context) => signRepository),
    RepositoryProvider<RegisterRepository>(
    create: (context) => registerRepository),
    ]*/
                            ],
                            child:*/ /*MultiBlocProvider(
                                providers: [
                                  BlocProvider<BottomNavigationBarBloc>(
                                    create: (_) => BottomNavigationBarBloc(),
                                  ),
                                ],
                                child*/
                OnBoard())));
  }
}
