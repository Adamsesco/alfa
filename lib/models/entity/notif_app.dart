import 'package:alpha/utils/styles/colors_app.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NotifApp {
  String icon;
  String title;
  String description;
  String date;
  Color color;

  NotifApp({this.description, this.date, this.icon, this.title, this.color});

  factory NotifApp.fromMap(Map<String, dynamic> map) {
    return NotifApp(
        color: ColorsApp.col_app,
        title: map["Title"],
        description: map["Description"],
        icon: "assets/icons/n_n.svg",
        date: map["Date"].toString().substring(0, 10));
  }
}
