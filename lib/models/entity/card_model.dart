class CardModel {
  var id;
  String label;
  String balance;
  String code;
  bool check;
  String name;

  CardModel(
      {this.id, this.name, this.check, this.balance, this.code, this.label});

  factory CardModel.fromMap(Map<String, dynamic> map) {
    return CardModel(
      check: false,
      balance: map["label"],
      code: map["number"],
      label: map["label"],
      id: map['id'] as String,
      name: map['name'] as String,
    );
  }
}
