
import 'package:meta/meta.dart';

class Category {
  final String id;
  final String name;
  bool check;

  Category({
    @required this.id,
    this.name,
    this.check
  });

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      check: false,
      id: map['id'] as String,
      name: map['name'] as String,
    );
  }

}

