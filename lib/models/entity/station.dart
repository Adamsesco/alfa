import 'package:alpha/utils/styles/colors_app.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class Station {
  String id;
  List<String> images;
  String name;
  String description;
  String type;
  String l_desc;
  String desc;
  String button;
  Color color;
  String small_desc;
  String image_preview;

  Station(
      {@required this.id = "",
      @required this.name = "",
      @required this.images = const [],
      this.type = "",
       this.color,
       this.description,
         this.small_desc,
       this.button,
       this.desc,
       this.image_preview,
      this.l_desc = ""});

  factory Station.fromMap(Map<String, dynamic> map) {
    return Station(
      color: map["color"],
      small_desc: map["small_desc"],
      button: map["button"],
      desc: map["desc"],
      image_preview: map["image_preview"],
      type: map["type"],
      l_desc: map["l_desc"],
      images: map.containsKey('images') == false
          ? [
              'https://res.cloudinary.com/dgxctjlpx/image/upload/v1618096841/placeholder_itp3gm.png'
            ]
          : map['images'] == null
              ? [""]
              : List<String>.from(map['images']),
      description: map['description'] as String,
      name: map['name'] as String,
    );
  }

  static List<Station> stations_items = [
    {
      'type': "CIvil ID Registration",
      "small_desc":
          "An exclusive feature in Alfa stations by using your Smart Civil ID.",
      "image_preview": "assets/images/back1.png",
      "desc":
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      "button": "Register your id",
      "color": ColorsApp.col_app,
      "id": "1",
      "name": "Our stations",
      "images": [
        "https://res.cloudinary.com/dgxctjlpx/image/upload/v1623950965/1067847739139239032498c4ea0c7892_ts9a4c.png"
      ],
      "description":
          "Alfa stations are owned by Soor Fuel Marketing Company, which is a …",
    },
    { 'type': "Transaction History",
      "small_desc":
      "An exclusive feature in Alfa stations by using your Smart Civil ID.",
      "image_preview": "assets/images/back3.png",
      "desc":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      "button": "Register your id",
      "color": const Color(0xff807F82),

      "id": "2",
      "name": "Car wash",
      "images": [
        "https://res.cloudinary.com/dgxctjlpx/image/upload/v1623950965/6cc708719e65d86cfc7c1c54a6f91878_rxl29k.png"
      ],
      "description":
          "Our service stations offer a range of quality car wash services…"
    },
    {
      'type': "CIvil ID Registration",
      "small_desc":
      "An exclusive feature in Alfa stations by using your Smart Civil ID.",
      "image_preview": "assets/images/back2.png",
      "desc":
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      "button": "Register your id",
      "color":  const Color(0xff807F82),
      "id": "3",
      "name": "Car services",
      "images": [
        "https://res.cloudinary.com/dgxctjlpx/image/upload/v1623951540/e10418914c5bc6a6c185cb4527c298ad_tzaggj.png"
      ],
      "description": "Lorem ipsum dolor sit amet, consectet adipiscing elit",
    }
  ].map((e) => Station.fromMap(e)).toList();
}
