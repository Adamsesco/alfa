import 'package:flutter/material.dart';

class Fuel {
  String id;
  String type;
  bool up;
  String type_fuel;
  String price;
  Color color;
  String pourcent;

  Fuel(
      {@required this.id = "",
      @required this.pourcent = "",
      @required this.type = "",
      @required this.up = false,
      this.color = Colors.white,
      @required this.type_fuel = "",
      this.price = ""});

  factory Fuel.fromMap(Map<String, dynamic> map) {
    return Fuel(
      pourcent: map['pourcent'] as String,
      id: map['id'] as String,
      up: map['up'] as bool,
      color: map['color'] ,
      type: map['type'] as String,
    );
  }

  static List<Fuel> list = [
    {
      "color": Color(0xffF9CD2E),
      "type":"DIESEL",
      "up":true,
      "pourcent":"0.2%"
    },
    {
      "color": Color(0xffC09339),
      "type":"SUPER",
      "up":true,
      "pourcent":"0.3%"
    },
    {
      "color": Color(0xff128B44),
      "type":"ULTRE SUPER",
      "up": false,
      "pourcent":"0.1%"
    }
  ].map((e) => Fuel.fromMap(e)).toList();
}
