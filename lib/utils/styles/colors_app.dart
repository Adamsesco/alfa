import 'package:flutter/material.dart';

class ColorsApp{

  static const Color  col_app = const Color(0xffF68F24);
  static const Color  col_app_f = const Color(0xffEB6E1F);
  static const Color  col_app_l = const Color(0xffF68F24);
  static Color col_app_yellow = const Color(0xffEDC92D);
  static Color col_app_brown = const Color(0xffC09339);
  static Color col_green_f = const Color(0xff288435);
  static Color col_app_brown_f = const Color(0xffC48520);

  static Color col_app_deep = const Color(0xffED6E00);
  static Color col_red = const Color(0xffFF0000);
  static Color col_green = const Color(0xff00B445);
  static Color col_grey_clear = const Color(0xffC9D2E0);
  static Color col_grey_text = const Color(0xff807F82);

  static Color col_black = const Color(0xff2B2926);
  static Color col_other_grey = const Color(0xffD9E1ED);
  static Color col_white_grey = const Color(0xffF0F2F5);
  static Color col_gr = const Color(0xffC9D2E0);
  static Color col_blue = const Color(0xff349BF4);

  static Color col_gr_back = const Color(0xffF0F2F5);



}